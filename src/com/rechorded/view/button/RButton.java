package com.rechorded.view.button;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.rechorded.R;
import com.rechorded.view.RPainter;

public class RButton extends Button {
	RPainter painter;
	
	public RButton(Context context) {
		super(context);
		init();
	}

	public RButton(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public RButton(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		painter = new RPainter(this);
		painter.useRedAndWhiteTheme();
		setBackgroundResource(R.drawable.blackbutton);
		setTextAppearance(getContext(), R.style.ButtonText);
		Typeface typeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/webissimo-regular.ttf");
		setTypeface(typeface);
		setSoundEffectsEnabled(false);
	}
	
    @Override
    protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);
   		painter.onDraw(canvas);
    }

	public void setBadge(String badge) {
		painter.setCornerBadge(badge);
		invalidate();
	}


}
