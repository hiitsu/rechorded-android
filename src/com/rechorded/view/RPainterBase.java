package com.rechorded.view;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.view.View;

public class RPainterBase {
	final Paint paintBackground = new Paint();
	final Paint paintForeground = new Paint();
	static protected Typeface typeface;
	View owner;
	float currentFontSize = 20;
	public RPainterBase(View owner) {
		super();
		this.owner = owner;
		if( typeface == null )
			typeface = Typeface.createFromAsset(owner.getContext().getAssets(), "fonts/webissimo-regular.ttf");
		paintForeground.setTypeface(typeface);
		useBlackAndWhiteTheme();
	}
	
	public void useBlackAndWhiteTheme() {
		paintBackground.setColor(Color.BLACK);
		paintBackground.setAlpha(160);
		paintBackground.setAntiAlias(true);

		paintForeground.setColor(Color.WHITE);
		paintForeground.setStrokeWidth(2);
		paintForeground.setAntiAlias(true);
		setTextSize(20);
	}
	public void useRedAndWhiteTheme() {
		paintBackground.setColor(Color.RED);
		paintBackground.setAlpha(255);
		paintForeground.setAntiAlias(true);
		
		paintForeground.setColor(Color.WHITE);
		paintForeground.setStrokeWidth(2);
		paintForeground.setAntiAlias(true);		
		setTextSize(12);
	}
	
	public void setTextSize(float px) {
		final float densityMultiplier = owner.getContext().getResources().getDisplayMetrics().density;
		currentFontSize = px;
		paintForeground.setTextSize(px * densityMultiplier);		
	}

	public View getOwner() {
		return owner;
	}

	public void setOwner(View owner) {
		this.owner = owner;
	}

	public Paint getPaintBackground() {
		return paintBackground;
	}

	public Paint getPaintForeground() {
		return paintForeground;
	}


}
