package com.rechorded.view;

import android.graphics.Canvas;
import android.view.View;

public class RPainter extends RPainterBase {
	public enum OverlayPosition {
		RightTop, FillOwner;
	}
	class Overlay {
		public String text;
		public OverlayPosition position;
		public Overlay(String text, OverlayPosition position) {
			super();
			this.text = text;
			this.position = position;
		}
	}
	Overlay overlay = null;
	String cornerBadge = null;


	public RPainter(View owner) {
		super(owner);
	}
	
    public void onDraw(Canvas canvas) {
    	if( overlay != null ) {
    		useBlackAndWhiteTheme();
    		float x,y,w,h;
    		if( overlay.position == OverlayPosition.FillOwner ) {
    			x = owner.getLeft();
    			y = owner.getTop();
    			w = owner.getWidth();
    			h = owner.getHeight();
    		} else {
    			w = owner.getWidth()/2;
    			h = owner.getHeight()/2;   			
    			x = w;
    			y = 0;
    		}
    		canvas.drawRect(x,y,x+w,y+h,paintBackground);
    		float textWidth = paintForeground.measureText(overlay.text);
    		canvas.drawText(overlay.text, x+w/2-(textWidth/2),y+h/2+currentFontSize/2, paintForeground);
    	}
    	if( cornerBadge != null && !cornerBadge.isEmpty() ) {
    		useRedAndWhiteTheme();
    		float textWidth = paintForeground.measureText(cornerBadge);
    		float ballRadius = textWidth/2 + 4;
    		float cx = owner.getWidth()-ballRadius;
    		float cy = ballRadius;
	        canvas.drawCircle(cx,cy,ballRadius,paintBackground);
	        canvas.drawText(cornerBadge, cx-(textWidth/2), cy+currentFontSize/2, paintForeground);
	   	}
    }

	public void setOverlay(String text) {
		setOverlay(text,OverlayPosition.RightTop);
	}
	public void setOverlay(String text,OverlayPosition position) {
		overlay = new Overlay(text,position);
		owner.invalidate();
	}

	public String getCornerBadge() {
		return cornerBadge;
	}

	public void setCornerBadge(String cornerBadge) {
		this.cornerBadge = cornerBadge;
	}


}
/*
 * 	RPainterTimer timer;
public void setOverlayText(String text,long millis){
	clearTimer();
	timer = new RPainterTimer(this);
	timer.execute(millis);
	owner.invalidate();
}
public void clearTimer() {
	if( timer != null ) {
		timer.cancel(true);
		timer = null;
		
	}
}*/