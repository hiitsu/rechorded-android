package com.rechorded.view.indicator;

import java.util.ArrayList;

import com.rechorded.activity.RActivity;
import com.rechorded.model.RFilterChangeListener;
import com.rechorded.view.gridview.ChordSelectionAdapter;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.widget.TextView;

public class RIndicator extends TextView implements RFilterChangeListener {
	final ArrayList<Integer> filters = new ArrayList<Integer>();
	public RIndicator(Context context) {
		super(context);
		init(context);
	}

	public RIndicator(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public RIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	private void init(Context context) {
	 	RActivity activity = (RActivity)context;
		activity.registerFilterChangeListener(this);
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/webissimo-regular.ttf");
		setTypeface(typeface);
		setGravity(Gravity.CENTER|Gravity.CENTER_VERTICAL);
	}

	public void filterChanged(int filterId, Object value, Cursor cursor) {
		if( filters.indexOf(filterId) != -1 )
			setText(value.toString());
	}
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		setTextSize(TypedValue.COMPLEX_UNIT_PX,h);
	}
	public ArrayList<Integer> getFilterList() {
		return filters;
	}

}
