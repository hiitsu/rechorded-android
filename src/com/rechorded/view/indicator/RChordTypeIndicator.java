package com.rechorded.view.indicator;

import com.rechorded.R;

import android.content.Context;
import android.util.AttributeSet;

public class RChordTypeIndicator extends RIndicator {

	public RChordTypeIndicator(Context context) {
		super(context);
		init();
	}

	public RChordTypeIndicator(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public RChordTypeIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	private void init() {
		//super.getFilterList().add(R.id.chordTypeSpinner);
	}

}
