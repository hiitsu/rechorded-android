package com.rechorded.view.indicator;

import com.rechorded.R;

import android.content.Context;
import android.util.AttributeSet;

public class RPianoFingerCountIndicator extends RIndicator {

	public RPianoFingerCountIndicator(Context context) {
		super(context);
		init();
	}

	public RPianoFingerCountIndicator(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public RPianoFingerCountIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	private void init() {
		//super.getFilterList().add(R.id.pianoFingerCountSlider);
		//super.getFilterList().add(R.id.fingerCountSpinner);
	}

}
