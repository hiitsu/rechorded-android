package com.rechorded.view.indicator;

import com.rechorded.R;

import android.content.Context;
import android.util.AttributeSet;

public class RDifficultyIndicator extends RIndicator {

	public RDifficultyIndicator(Context context) {
		super(context);
		init();
	}

	public RDifficultyIndicator(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public RDifficultyIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	private void init() {
		//super.getFilterList().add(R.id.difficultySlider);
		//super.getFilterList().add(R.id.difficultySpinner);
	}

}
