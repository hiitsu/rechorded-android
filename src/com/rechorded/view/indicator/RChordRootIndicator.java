package com.rechorded.view.indicator;

import com.rechorded.R;

import android.content.Context;
import android.util.AttributeSet;

public class RChordRootIndicator extends RIndicator {

	public RChordRootIndicator(Context context) {
		super(context);
		init();
	}

	public RChordRootIndicator(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public RChordRootIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	private void init() {
		//super.getFilterList().add(R.id.chordRootSpinner);
	}

}
