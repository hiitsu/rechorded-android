package com.rechorded.view.indicator;

import com.rechorded.R;

import android.content.Context;
import android.util.AttributeSet;

public class RBarreIndicator extends RIndicator {

	public RBarreIndicator(Context context) {
		super(context);
		init();
	}

	public RBarreIndicator(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public RBarreIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	private void init() {
		//super.getFilterList().add(R.id.barreSlider);
		//super.getFilterList().add(R.id.barreSpinner);
	}

}
