package com.rechorded.view.indicator;

import com.rechorded.R;

import android.content.Context;
import android.util.AttributeSet;

public class RFingerCountIndicator extends RIndicator {

	public RFingerCountIndicator(Context context) {
		super(context);
		init();
	}

	public RFingerCountIndicator(Context context, AttributeSet attrs,
			int defStyle) {
		super(context, attrs, defStyle);
		init();
	}

	public RFingerCountIndicator(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}
	private void init() {
		//super.getFilterList().add(R.id.fingerCountSlider);
		//super.getFilterList().add(R.id.fingerCountSpinner);
	}

}
