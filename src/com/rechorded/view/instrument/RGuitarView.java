package com.rechorded.view.instrument;

import java.util.ArrayList;
import java.util.Collection;

import com.rechorded.activity.RActivity;
import com.rechorded.filter.RChordRootFilter;
import com.rechorded.filter.RChordTypeFilter;
import com.rechorded.filter.RFilter;
import com.rechorded.model.Chord;
import com.rechorded.model.Data;
import com.rechorded.model.GuitarChord;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class RGuitarView extends RWebView implements OnTouchListener  {

	GuitarChord guitarChord = new GuitarChord(0,"maj",0,"X32010","standard");
	
	public RGuitarView(Context context) {
		super(context,RWebView.GUITAR);
		init();
	}

	public RGuitarView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle,RWebView.GUITAR);
		init();
	}

	public RGuitarView(Context context, AttributeSet attrs) {
		super(context, attrs,RWebView.GUITAR);
		init();
	}

	private void init(){
		setOnTouchListener(this);
	}

	@Override	
	public boolean onTouch(View v, MotionEvent event) {
		if( event.getAction() == MotionEvent.ACTION_DOWN ) {
			RActivity activity = (RActivity)getContext();
			activity.playChord(guitarChord);
			return true;
		}
		return (event.getAction() == MotionEvent.ACTION_MOVE);
	}

	@Override
	public void selectionChanged(Chord chord) {
		if( chord instanceof GuitarChord ) {
			guitarChord = (GuitarChord)chord;
		} else {
			Collection<RFilter> a = new ArrayList<RFilter>();
			a.add(new RChordRootFilter(""+chord.getRoot()));
			a.add(new RChordTypeFilter(chord.getType()));
			guitarChord = Data.getInstance(getContext()).nthGuitarChord(a,0);
		}
		setChord(guitarChord);
	}
	
	@Override
	public void changeChordRoot(int root) {
		Collection<RFilter> a = new ArrayList<RFilter>();
		a.add(new RChordRootFilter(""+root));
		a.add(new RChordTypeFilter(guitarChord.getType()));
		GuitarChord g = Data.getInstance(getContext()).nthGuitarChord(a,0);
		if( g == null )
			guitarChord.setRoot(root);
		else  
			guitarChord = g;
		setChord(g);
		
	}

	@Override
	public void changeChordType(String type) {
		Collection<RFilter> a = new ArrayList<RFilter>();
		a.add(new RChordRootFilter(""+guitarChord.getRoot()));
		a.add(new RChordTypeFilter(type));
		GuitarChord g = Data.getInstance(getContext()).nthGuitarChord(a,0);
		if( g == null )
			guitarChord.setType(type);
		else
			guitarChord = g;
		setChord(g);
		
	}
	@Override
	public Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		bundle.putParcelable("instanceState", super.onSaveInstanceState());
		bundle.putSerializable("guitarChord",guitarChord);
		return bundle;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			guitarChord = (GuitarChord)bundle.getSerializable("guitarChord");
			setChord(guitarChord);
			super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
			return;
		}
		super.onRestoreInstanceState(state);
	}

	@Override
	public void updateUI() {
		setCanvasSize(getWidth(),getHeight());
		setTuning(guitarChord.getTuning());
		setChord(guitarChord);
		
	}
}
