package com.rechorded.view.instrument;

import com.rechorded.activity.RActivity;
import com.rechorded.model.Chord;
import com.rechorded.model.GuitarChord;
import com.rechorded.model.RSelectionChangeListener;
import com.rechorded.view.RPainter;
import com.rechorded.view.RPainter.OverlayPosition;

import android.annotation.SuppressLint;
import android.content.Context;

import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.Log;

import android.webkit.WebSettings;
import android.webkit.WebSettings.ZoomDensity;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.RenderPriority;


public abstract class RWebView extends WebView implements RSelectionChangeListener {
	private static final String 
		ASSET_BASE = "file:///android_asset/",
		TAG = "RWebView";
	
	public static String 
		GUITAR ="app-guitar.html", 
		PIANO ="app-piano.html";
	
	String assetFile = GUITAR;
	RPainter painter;
	RActivity activity;
	Handler handler;
	
	public RWebView(Context context) {
		super(context);
		init(context);
	}
	public RWebView(Context context,String assetFile){
		super(context);
		this.assetFile = assetFile;
		init(context);
	}
	
	public RWebView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}
	public RWebView(Context context, AttributeSet attrs, int defStyle,String assetFile) {
		super(context, attrs, defStyle);
		this.assetFile = assetFile;
		init(context);
	}

	public RWebView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	public RWebView(Context context, AttributeSet attrs,String assetFile) {
		super(context, attrs);
		this.assetFile = assetFile;
		init(context);
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void init(Context context) {
		setVisibility(INVISIBLE);
		activity = (RActivity)context;
		activity.registerSelectionChangeListener(this);
		
		painter = new RPainter(this);
		
		handler = new DelayedDraw(this);
		
		final WebView webView = this;
		WebSettings settings = webView.getSettings();
		settings.setJavaScriptEnabled(true);
		settings.setBuiltInZoomControls(false);
		settings.setSupportZoom(false);
		settings.setUseWideViewPort(true);
		settings.setLoadWithOverviewMode(true);
		settings.setDefaultZoom(ZoomDensity.FAR);
		settings.setCacheMode(WebSettings.LOAD_NO_CACHE);
		settings.setRenderPriority(RenderPriority.HIGH);
		settings.setAllowFileAccess(true);

        webView.setVerticalScrollBarEnabled(false);
        //webView.addJavascriptInterface(javascriptBridge,"app");
        webView.setWebViewClient(new WebViewClient() {
        	@SuppressWarnings("unused")
			public void onConsoleMessage(String message, int lineNumber, String sourceID) {
        		Log.d(TAG, message + " -- From line " + lineNumber + " of " + sourceID);
        	}
			public boolean shouldOverrideUrlLoading(WebView view, String url){
				Log.d(TAG,"shouldOverrideUrlLoading:"+url);
		        return false;
		    }
			public void onPageFinished(WebView view, String url) {
				Log.d(TAG,"onPageFinished:"+url);
				handler.sendMessageDelayed(new Message(),200);
			}
        });
        webView.setBackgroundColor(Color.parseColor("#000000"));
        if( !isInEditMode() )
        	webView.loadUrl(ASSET_BASE+assetFile);
	}

    @Override
    protected void onDraw(Canvas canvas) {
    	super.onDraw(canvas);
    	painter.onDraw(canvas);
    }
    
    @Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		Log.d(TAG,"onSizeChanged to "+w+","+h);
		if( w == 0 || h == 0 )
			return;
		updateUI();
	}
	protected void setTuning(String tuning){
		String call = "javascript:R.setTuning('"+tuning+"');";
		loadUrl(call);
		Log.d(TAG,call);
	}  
	protected void setCanvasSize(int w,int h){
		String callResize = "javascript:R.setSize("+w+","+h+");";
		Log.d(TAG,callResize);
		loadUrl(callResize);
	}
	protected void setChord(Chord chord){
		String call;
		if( chord == null ) {
			call = "javascript:R.setChord(null,null);";
			painter.setOverlay("Not Found",RPainter.OverlayPosition.FillOwner);
		} else if( chord instanceof GuitarChord ) {
			GuitarChord g = (GuitarChord) chord;
			setTuning(g.getTuning());
			call = "javascript:R.setChord("+g.getRoot()+",'"+g.getType()+"',"+g.getBarre()+",'"+g.getFingers()+"');";
			painter.setOverlay(g.toString());
		} else {
			call = "javascript:R.setChord("+chord.getRoot()+",'"+chord.getType()+"');";
			painter.setOverlay(chord.toString());
		}
		Log.d(TAG,call);
		loadUrl(call);
	}
	@Override
	public Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		bundle.putParcelable("instanceState", super.onSaveInstanceState());
		return bundle;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
			return;
		}
		super.onRestoreInstanceState(state);
	}
	public abstract void updateUI();

}
/*
 * 	public void setScale(int root,String type){
		String call = "javascript:R.setScale("+root+",'"+type+"');";
		Log.d(TAG,call);
		loadUrl(call);
	}
	
	public void setShowScale(boolean flag) {
		if( !flag ) {
			String call = "javascript:R.setScale(null,null);";
			loadUrl(call);
		}
	}
 	public void selectionChanged(String selection) {
		if( selection == null || selection.isEmpty() ) {
			Log.w(TAG,"selection = "+selection);
			return;
		}
		String a[] = selection.split(Data.VALUE_SEP);
		String chordText = Data.toRoot(a[0])+a[1];

		chordSelection = selection;
		
		if( a.length > 2)
			setChord(a[0],a[1],a[2],a[3]);
		else
			setChord(a[0],a[1]);
	}
 * 	public boolean onTouch(View v, MotionEvent event) {
		if( event.getAction() == MotionEvent.ACTION_DOWN ) {
			String a[] = chordSelection.split(Data.VALUE_SEP);
			activity.playChord(a[0],a[1]);
			selectionChanged(chordSelection);
			return true;
		}
		return (event.getAction() == MotionEvent.ACTION_MOVE);
	}	
 * public void selectionChanged(Cursor cursor) {
if( cursor.getColumnCount() > 2 )
	selectionChanged(Data.cursorToString(cursor,new int[]{0,1,2,3}));
else
	selectionChanged(Data.cursorToString(cursor,new int[]{0,1}));

}*/

