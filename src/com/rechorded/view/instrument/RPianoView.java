package com.rechorded.view.instrument;


import com.rechorded.activity.RActivity;
import com.rechorded.model.Chord;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;

public class RPianoView extends RWebView implements OnTouchListener {
	
	Chord chord = new Chord();
	
	public RPianoView(Context context) {
		super(context,PIANO);
		init();
	}

	public RPianoView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle,PIANO);
		init();
	}

	public RPianoView(Context context, AttributeSet attrs) {
		super(context, attrs,PIANO);
		init();
	}

	public RPianoView(Context context, String assetFile) {
		super(context, PIANO);
		init();
	}
	
	private void init(){
		setOnTouchListener(this);
	}


	@Override
	public Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		bundle.putParcelable("instanceState", super.onSaveInstanceState());
		bundle.putSerializable("chord",chord);
		return bundle;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			chord = (Chord)bundle.getSerializable("chord");
			setChord(chord);
			super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
			return;
		}
		super.onRestoreInstanceState(state);
	}
	
	@Override	
	public boolean onTouch(View v, MotionEvent event) {
		if( event.getAction() == MotionEvent.ACTION_DOWN ) {
			RActivity activity = (RActivity)getContext();
			activity.playChord(chord);
			return true;
		}
		return (event.getAction() == MotionEvent.ACTION_MOVE);
	}
	
	@Override
	public void selectionChanged(Chord c) {
		this.chord = c;
		setChord(c);
	}
	@Override
	public void changeChordRoot(int root) {
		chord.setRoot(root);
		setChord(chord);
	}

	@Override
	public void changeChordType(String type) {
		chord.setType(type);
		setChord(chord);
	}
	@Override
	public void updateUI() {
		setCanvasSize(getWidth(),getHeight());
		setChord(chord);
	}
}
