package com.rechorded.view.instrument;

import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;

public class DelayedDraw extends Handler {
	RWebView owner;
	private String TAG = DelayedDraw.class.getSimpleName();
	public DelayedDraw(RWebView owner) {
		super();
		this.owner = owner;
	}

	@Override
	public void handleMessage(Message message) {
		Log.d(TAG,"setVisibility:VISIBLE and updateUI");
		owner.setVisibility(View.VISIBLE);
		owner.updateUI();
	}
}