package com.rechorded.view.gridview;


import java.util.HashMap;

import com.rechorded.R;
import com.rechorded.activity.RActivity;


import android.content.Context;

import android.os.Bundle;
import android.os.Parcelable;

import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.AdapterView.OnItemClickListener;

public class RChordGridView extends GridView implements OnItemSelectedListener, OnItemClickListener{
	final String TAG = "RChordGridView";
	ChordSelectionAdapter adapter;
	int optimalControlSize = 90;
	
	public RChordGridView(Context context) {
		super(context);
		init(context);
	}

	public RChordGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public RChordGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	void init(Context context){
		setVerticalSpacing(10);
		RActivity activity = (RActivity)context;
		this.optimalControlSize = activity.getOptimalWidth();
		adapter = new ChordSelectionAdapter(context,optimalControlSize);
		adapter.setParent(this);
		if( this.getId() == R.id.pianoChordGridView )
			adapter.setEnableBadging(false);
		else 
			adapter.setEnableBadging(true);
		setAdapter(adapter);
		setSelection(0);
		activity.registerFilterChangeListener(adapter);
		
	}
	
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if( w == 0 || h == 0 )
			return;
		int c = w/optimalControlSize;
		setNumColumns(c);
	}
	
	@Override
	public Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		bundle.putParcelable("instanceState", super.onSaveInstanceState());
		bundle.putSerializable("inversionMap",adapter.getInversionMap());
		bundle.putInt("selectedIndex",adapter.getSelectedIndex());
		return bundle;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			adapter.setInversionMap((HashMap<Integer, Integer>) bundle.getSerializable("inversionMap"));
			adapter.setSelectedIndex(bundle.getInt("selectedIndex"));
			super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
			return;
		}
		super.onRestoreInstanceState(state);
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		Log.d(TAG,"onItemSelected:"+position);
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		Log.d(TAG,"onNothingSelected:"+parent);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id){
		Log.d(TAG,"onItemClick:"+position);
		//if( selectionPosition != -1 )
		//	getSelectedView()
		//parent.setSelection(position);
		//view.setBackgroundResource(R.drawable.redbutton);
		
	}
}

/*
 *     @Override
    protected void dispatchDraw(Canvas canvas) {
        super.dispatchDraw(canvas);
        if( loading ) {
	    	String text ="querying and regenerating controls";
	        canvas.drawRect(0,0,getWidth(),getHeight(), paintBackground); 
			float textWidth = paintForeground.measureText(text);
			float cx = getWidth()/2;
			float cy = getHeight()/2;
	        canvas.drawText(text, cx-(textWidth/2), cy+30/2, paintForeground);
        }
    }
    private static ArrayList<View> getViewsByTag(ViewGroup root, String tag){
        ArrayList<View> views = new ArrayList<View>();
        final int childCount = root.getChildCount();
        for (int i = 0; i < childCount; i++) {
            final View child = root.getChildAt(i);
            if (child instanceof ViewGroup) {
                views.addAll(getViewsByTag((ViewGroup) child, tag));
            } 

            final Object tagObj = child.getTag();
            if (tagObj != null && tagObj.equals(tag)) {
                views.add(child);
            }

        }
        return views;
    }
 * 	public void changed(Cursor cursor) {
		loading = true;
		adapter.changed(cursor);
		invalidateViews();
		loading = false;
		//Toast.makeText(getContext(), "Has now "+map.size()+" items",Toast.LENGTH_SHORT).show();
	}
 * @Override
protected void onDraw(Canvas canvas) {
	super.onDraw(canvas);
	String text ="loading";
    canvas.drawRect(0,0,getWidth(),getHeight(), paintBackground); 
	float textWidth = paintForeground.measureText(text);
	float cx = getWidth()/2;
	float cy = getHeight()/2;
    canvas.drawText(text, cx-(textWidth/2), cy+30/2, paintForeground);
}*/
