package com.rechorded.view.gridview;

import com.rechorded.R;

import android.content.Context;
import android.content.res.Configuration;

import android.graphics.Color;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.util.Log;

import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ScreenListingAdapter extends BaseAdapter {
	private static final String IMAGE_VIEW_TAG = "ImageViewTag",
			LABEL_TAG = "LabelTag";
	final String TAG = "ScreenListingAdapter";
	private Context context;
	int optimalWidth,optimalHeight;
	
	public ScreenListingItem[] items = new ScreenListingItem[]{
		new ScreenListingItem(R.drawable.icon_beginner,"Guitar: Easy Chords","RActivity",R.layout.beginnerguitar,"REasyChordFilter"),
		new ScreenListingItem(R.drawable.icon_difficulty,"Guitar: Chords By Difficulty","RActivity",R.layout.chordsbydifficulty),
		new ScreenListingItem(R.drawable.chords_by_barre,"Guitar: Chords By Barre","RActivity",R.layout.chordsbybarre),
		new ScreenListingItem(R.drawable.chords_by_finger_usage,"Guitar: Chords By Finger Usage","RActivity",R.layout.chordsbyfingercount),
		new ScreenListingItem(R.drawable.chords_by_root,"Guitar: Chords By Root","RActivity",R.layout.chordsbyroot),
		new ScreenListingItem(R.drawable.chords_by_type,"Guitar: Chords By Type","RActivity",R.layout.chordsbytype),
		new ScreenListingItem(R.drawable.piano_chords_by_finger_usage,"Piano: Chords by Note Count","RActivity",R.layout.pianochordsbynotecount),
		new ScreenListingItem(R.drawable.piano_chords_by_root_v2,"Piano: Chords by Root","RActivity",R.layout.pianochordsbyroot),
		new ScreenListingItem(R.drawable.dual,"Piano & Guitar","RActivity",R.layout.duet),
	};
	public ScreenListingAdapter(Context context){
		super();
		this.context = context;
	}

	public int getCount() {
		return items.length;
	}

	
	public Object getItem(int position) {
		return items[position];
	}
	
	public long getItemId(int position) {
		return items.hashCode();
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		ScreenListingItem item = items[position];
		if( convertView != null && convertView instanceof LinearLayout ) {
			ImageView imageView = (ImageView)convertView.findViewWithTag(IMAGE_VIEW_TAG);
			imageView.setImageResource(item.drawable);
			TextView label = (TextView)convertView.findViewWithTag(LABEL_TAG);
			label.setText(item.getText());
			Log.d(TAG,"Recycled view for item at "+position);
			return convertView;
		} else {
			LinearLayout layout = new LinearLayout(context);
			layout.setOrientation(LinearLayout.VERTICAL);
			layout.setGravity(Gravity.CENTER);
			layout.setWeightSum(6f);
			ImageView imageView = new ImageView(context);
			imageView.setBackgroundColor(Color.parseColor("#f0f0f0"));
			imageView.setImageResource(item.drawable);
			imageView.setTag(IMAGE_VIEW_TAG);
			imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
			LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0,4f);
			imageParams.setMargins(5,5,5,0);
			layout.addView(imageView,imageParams);
			
			TextView label = new TextView(context);
			Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/webissimo-regular.ttf");
			label.setTextColor(Color.BLACK);
			label.setTypeface(typeface);
			label.setTag(LABEL_TAG);
			label.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP);
			label.setText(item.getText());
			LinearLayout.LayoutParams labelParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0,2f);
			layout.addView(label,labelParams);
			layout.setLayoutParams(new GridView.LayoutParams(optimalWidth,optimalHeight));
			Log.d(TAG,"Created new view for item at "+position);
			return layout;
		}
	}

	public int getOptimalWidth() {
		return optimalWidth;
	}

	public void setOptimalWidth(int optimalWidth) {
		this.optimalWidth = optimalWidth;
	}

	public int getOptimalHeight() {
		return optimalHeight;
	}

	public void setOptimalHeight(int optimalHeight) {
		this.optimalHeight = optimalHeight;
	}
	
}