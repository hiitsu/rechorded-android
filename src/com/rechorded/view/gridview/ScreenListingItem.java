package com.rechorded.view.gridview;

public class ScreenListingItem {
	static public final String 
		LAYOUT_ID = "layoutId", 
		FILTER_EXTRA = "extraFilter";
	
	int drawable;
	String text;
	String activityClassName;
	int layoutId;
	String extraFilterClass;
	public ScreenListingItem(int drawable, String text,
			String activityClassName, int layoutId) {
		super();
		this.drawable = drawable;
		this.text = text;
		this.activityClassName = activityClassName;
		this.layoutId = layoutId;
	}
	
	public ScreenListingItem(int drawable, String text,
			String activityClassName, int layoutId, String extraFilterClass) {
		super();
		this.drawable = drawable;
		this.text = text;
		this.activityClassName = activityClassName;
		this.layoutId = layoutId;
		this.extraFilterClass = extraFilterClass;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((activityClassName == null) ? 0 : activityClassName
						.hashCode());
		result = prime * result + drawable;
		result = prime * result + layoutId;
		result = prime * result + ((text == null) ? 0 : text.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ScreenListingItem other = (ScreenListingItem) obj;
		if (activityClassName == null) {
			if (other.activityClassName != null)
				return false;
		} else if (!activityClassName.equals(other.activityClassName))
			return false;
		if (drawable != other.drawable)
			return false;
		if (layoutId != other.layoutId)
			return false;
		if (text == null) {
			if (other.text != null)
				return false;
		} else if (!text.equals(other.text))
			return false;
		return true;
	}
	public int getDrawable() {
		return drawable;
	}
	public void setDrawable(int drawable) {
		this.drawable = drawable;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public String getActivityClassName() {
		return activityClassName;
	}
	public void setActivityClassName(String activityClassName) {
		this.activityClassName = activityClassName;
	}
	public int getLayoutId() {
		return layoutId;
	}
	public void setLayoutId(int layoutId) {
		this.layoutId = layoutId;
	}

	public String getExtraFilterClass() {
		return extraFilterClass;
	}

	public void setExtraFilter(String extraFilterClass) {
		this.extraFilterClass = extraFilterClass;
	}
	
}
