package com.rechorded.view.gridview;

import com.rechorded.R;

import android.content.Context;


import android.graphics.Color;
import android.graphics.Typeface;


import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.BaseAdapter;

import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

public class SectionedScreenListingAdapter extends BaseAdapter {
	private static final String IMAGE_VIEW_TAG = "ImageViewTag",
			LABEL_TAG = "LabelTag";
	static final String TAG = SectionedScreenListingAdapter.class.getSimpleName();
	private Context context;
	int optimalWidth,optimalHeight;
	
	public Object[] items = new Object[]{
			"Basics",
			new ScreenListingItem[] {
				new ScreenListingItem(R.drawable.icon_beginner,"Guitar: Easy Chords","RActivity",R.layout.beginnerguitar,"REasyChordFilter"),
				new ScreenListingItem(R.drawable.icon_difficulty,"Guitar: Chords By Difficulty","RActivity",R.layout.chordsbydifficulty),
				new ScreenListingItem(R.drawable.chords_by_finger_usage,"Guitar: Chords By Finger Usage","RActivity",R.layout.chordsbyfingercount),
				new ScreenListingItem(R.drawable.chords_by_barre,"Guitar: Chords By Barre","RActivity",R.layout.chordsbybarre)
			},
			"Advanced",
			new ScreenListingItem[] {
				new ScreenListingItem(R.drawable.chords_by_root,"Guitar: Chords By Root","RActivity",R.layout.chordsbyroot),
				new ScreenListingItem(R.drawable.chords_by_type,"Guitar: Chords By Type","RActivity",R.layout.chordsbytype),
				new ScreenListingItem(R.drawable.piano_chords_by_finger_usage,"Piano: Chords by Note Count","RActivity",R.layout.pianochordsbynotecount),
				new ScreenListingItem(R.drawable.piano_chords_by_root_v2,"Piano: Chords by Root","RActivity",R.layout.pianochordsbyroot),
				new ScreenListingItem(R.drawable.dual,"Piano & Guitar","RActivity",R.layout.duet),
			},
			"Scales",
			new ScreenListingItem[] {
				new ScreenListingItem(R.drawable.chords_by_root,"Guitar: Chords By Root","RActivity",R.layout.chordsbyroot),
				new ScreenListingItem(R.drawable.chords_by_type,"Guitar: Chords By Type","RActivity",R.layout.chordsbytype),
				new ScreenListingItem(R.drawable.piano_chords_by_finger_usage,"Piano: Chords by Note Count","RActivity",R.layout.pianochordsbynotecount),
				new ScreenListingItem(R.drawable.piano_chords_by_root_v2,"Piano: Chords by Root","RActivity",R.layout.pianochordsbyroot),
				new ScreenListingItem(R.drawable.dual,"Piano & Guitar","RActivity",R.layout.duet),
			},
			"Compose",
			new ScreenListingItem[] {
				new ScreenListingItem(R.drawable.chords_by_root,"Guitar: Chords By Root","RActivity",R.layout.chordsbyroot),
				new ScreenListingItem(R.drawable.chords_by_type,"Guitar: Chords By Type","RActivity",R.layout.chordsbytype),
				new ScreenListingItem(R.drawable.piano_chords_by_finger_usage,"Piano: Chords by Note Count","RActivity",R.layout.pianochordsbynotecount),
				new ScreenListingItem(R.drawable.piano_chords_by_root_v2,"Piano: Chords by Root","RActivity",R.layout.pianochordsbyroot),
				new ScreenListingItem(R.drawable.dual,"Piano & Guitar","RActivity",R.layout.duet),
			}
	};
	public SectionedScreenListingAdapter(Context context){
		super();
		this.context = context;
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
		optimalWidth = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,160,metrics));
		optimalHeight = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,160,metrics));
	}

	public int getCount() {
		return items.length;
	}
	
	public Object getItem(int position) {
		return items[position];
	}
	
	public long getItemId(int position) {
		return position;
	}
    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return items[position] instanceof String ? 0 : 1;
    }	
	public View getView(int position, View convertView, ViewGroup parent) {
		Object item = items[position];
		if( item instanceof String ) {
			TextView textView = null;
			if( convertView instanceof TextView ) {
				textView = (TextView)convertView;
			} else {
				textView = new TextView(context);
			}
			textView.setText((String)item);
			return textView;
		} else {
			HorizontalScrollView scrollView = new HorizontalScrollView(context);
			LinearLayout linearLayout = new LinearLayout(context);
			linearLayout.setOrientation(LinearLayout.HORIZONTAL);
			linearLayout.setGravity(Gravity.LEFT);
			ScreenListingItem[] items = (ScreenListingItem[])item;
			for( ScreenListingItem listingItem : items ) {
					linearLayout.addView(labeledPictureFor(listingItem.drawable,listingItem.text),new LinearLayout.LayoutParams(160,160));
			}
			scrollView.addView(linearLayout,new ListView.LayoutParams(LayoutParams.MATCH_PARENT,optimalHeight+20));
			return scrollView;
			//HorizontalScrollView scrollView = new HorizontalScrollView(context);
			//LinearLayout linearLayout = new LinearLayout(context);
			//linearLayout.setOrientation(LinearLayout.HORIZONTAL);
			//linearLayout.setGravity(Gravity.LEFT);
			//for( ScreenListingItem listingItem : items)
			//	linearLayout.addView(labeledPictureFor(listingItem.drawable,listingItem.text),new LinearLayout.LayoutParams(optimalWidth,LayoutParams.WRAP_CONTENT));
			//linearLayout.setLayoutParams(new ListView.LayoutParams(optimalWidth,LayoutParams.WRAP_CONTENT));
			//return linearLayout;
			//scrollView.addView(linearLayout);
			//return scrollView;
		}
	}
	private View labeledPictureFor(int drawable,String text) {
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.VERTICAL);
		layout.setEnabled(false);
		layout.setGravity(Gravity.CENTER);
		layout.setWeightSum(6f);
		ImageView imageView = new ImageView(context);
		imageView.setBackgroundColor(Color.parseColor("#f0f0f0"));
		imageView.setImageResource(drawable);
		imageView.setTag(IMAGE_VIEW_TAG);
		imageView.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
		LinearLayout.LayoutParams imageParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0,4f);
		imageParams.setMargins(5,5,5,0);
		layout.addView(imageView,imageParams);
		
		TextView label = new TextView(context);
		Typeface typeface = Typeface.createFromAsset(context.getAssets(), "fonts/webissimo-regular.ttf");
		label.setTextColor(Color.BLACK);
		label.setTypeface(typeface);
		label.setTag(LABEL_TAG);
		label.setGravity(Gravity.CENTER_HORIZONTAL|Gravity.TOP);
		label.setText(text);
		LinearLayout.LayoutParams labelParams = new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,0,2f);
		layout.addView(label,labelParams);
		layout.setLayoutParams(new ListView.LayoutParams(optimalWidth,optimalHeight));
		return layout;
	}
	public int getOptimalWidth() {
		return optimalWidth;
	}

	public void setOptimalWidth(int optimalWidth) {
		this.optimalWidth = optimalWidth;
	}

	public int getOptimalHeight() {
		return optimalHeight;
	}

	public void setOptimalHeight(int optimalHeight) {
		this.optimalHeight = optimalHeight;
	}
	
}