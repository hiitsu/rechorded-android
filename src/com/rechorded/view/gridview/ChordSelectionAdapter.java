package com.rechorded.view.gridview;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import com.rechorded.R;
import com.rechorded.activity.RActivity;
import com.rechorded.filter.RChordRootFilter;
import com.rechorded.filter.RChordTypeFilter;
import com.rechorded.filter.RFilter;
import com.rechorded.model.Chord;
import com.rechorded.model.Data;
import com.rechorded.model.RFilterChangeListener;
import com.rechorded.view.button.RButton;

import android.annotation.SuppressLint;
import android.content.Context;

import android.database.Cursor;

import android.util.Log;

import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

import android.widget.CursorAdapter;
import android.widget.GridView;


public class ChordSelectionAdapter extends CursorAdapter implements RFilterChangeListener, OnClickListener {

	private static final String TAG = ChordSelectionAdapter.class.getSimpleName();
	private final static int SELECTED_ID = 123, DUMMY_ID = 1000,
		NO_SELECTION = -2;
	int optimalControlSize = 60;
	int selectedIndex = NO_SELECTION;

	Context context;
	View parent;

	boolean enableBadging = true;
	@SuppressLint("UseSparseArrays")
	HashMap<Integer,Integer> inversionMap = new HashMap<Integer,Integer>();

	public ChordSelectionAdapter(Context context, Cursor c, boolean autoRequery) {
		super(context, c, autoRequery);
		this.context = context;
	}

	public ChordSelectionAdapter(Context context) {
		super(context,null,false);
		this.context = context;
	}
	public ChordSelectionAdapter(Context context,int optimalControlSize) {
		super(context,null,false);
		this.optimalControlSize = optimalControlSize;
		this.context = context;
	}
	
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		RButton button = new RButton(context);
		button.setOnClickListener(this);
		bindView(button,context,cursor);
		button.setLayoutParams(new GridView.LayoutParams(optimalControlSize,optimalControlSize));		
		return button;
	}
	
	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		RButton button = (RButton)view;
		button.setText(Data.NOTES[cursor.getInt(0)]+cursor.getString(1));
		
		if( selectedIndex == cursor.getPosition() ) {
			button.setId(SELECTED_ID);
			button.setBackgroundResource(R.drawable.bluebutton);
		} else {
			button.setId(DUMMY_ID);
			button.setBackgroundResource(R.drawable.blackbutton);
		}
		
		if( enableBadging && cursor.getColumnCount() > 3 && cursor.getInt(3) > 1 ) {
			Integer o = inversionMap.get(cursor.getPosition());
			int next = (o == null) ? 0 : o;
			button.setTag(new Object[] { cursor.getPosition(),cursor.getInt(0),cursor.getString(1),cursor.getInt(3), next} );
			button.setBadge((next+1)+"/"+cursor.getInt(3));
		} else {
			button.setTag(new Object[] { cursor.getPosition(),cursor.getInt(0),cursor.getString(1) } );
			button.setBadge(null);
		}
		
	}
	
	public void onClick(View view) {
		Log.d(TAG,"clicked view with id/hash: "+view.getId());

		RButton button = (RButton)view;
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(context);
		Object[] a = (Object[]) view.getTag();
		if( selectedIndex != NO_SELECTION ) {
			View selectedView = parent.findViewById(SELECTED_ID);
			if( selectedView != null ) {
				selectedView.setBackgroundResource(R.drawable.blackbutton);
				selectedView.setId(DUMMY_ID);
			}
		}
		selectedIndex = (Integer) a[0];
		button.setId(SELECTED_ID);
		button.setBackgroundResource(R.drawable.bluebutton);
		Chord chord = new Chord((Integer)a[1],(String)a[2]);
		if( enableBadging && a.length > 3 ) {
			int m = (Integer) a[3];
			int n = (Integer) a[4];
			int next = n+1;
			if( next >= m )
				next = 0;
			inversionMap.put((Integer) a[0],n);
			a[4] = next;
			button.setBadge((n+1)+"/"+m);
			Collection<RFilter> filters = new ArrayList<RFilter>();
			filters.add(new RChordRootFilter(""+chord.getRoot()));
			filters.add(new RChordTypeFilter(chord.getType()));
			filters.addAll(activity.getFilters().values());
			chord = data.nthGuitarChord(filters,n);
			if( chord == null ) {
				Log.e(TAG,"chord should not be null here!");
			}
		}
		activity.notifySelectionChanged(chord);
	}
	public void filterChanged(int filterId, Object value,Cursor cursor) {
		inversionMap.clear();
		selectedIndex = NO_SELECTION;
		super.changeCursor(cursor);
	}

	public HashMap<Integer, Integer> getInversionMap() {
		return inversionMap;
	}

	public void setInversionMap(HashMap<Integer, Integer> inversionMap) {
		this.inversionMap = inversionMap;
	}
	public boolean isEnableBadging() {
		return enableBadging;
	}

	public void setEnableBadging(boolean enableBadging) {
		this.enableBadging = enableBadging;
	}

	public int getSelectedIndex() {
		return selectedIndex;
	}

	public void setSelectedIndex(int selectedIndex) {
		this.selectedIndex = selectedIndex;
	}
	public View getParent() {
		return parent;
	}

	public void setParent(View parent) {
		this.parent = parent;
	}

}
