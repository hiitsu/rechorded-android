package com.rechorded.view.gridview;

import com.rechorded.activity.RActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;


public class RScreenGridView extends GridView {
	Context context;
	public RScreenGridView(Context context) {
		super(context);
		init(context);
	}

	public RScreenGridView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public RScreenGridView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context c) {
		this.context = c;
        final GridView gridView = this;
        gridView.setGravity(Gravity.CENTER);
        DisplayMetrics metrics = getResources().getDisplayMetrics();
		int optimalButtonWidth = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,160,metrics));
		int optimalButtonHeight = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,160,metrics));
		ScreenListingAdapter adapter = new ScreenListingAdapter(context);
		adapter.setOptimalWidth(optimalButtonWidth);
		adapter.setOptimalHeight(optimalButtonHeight);
		gridView.setNumColumns(metrics.widthPixels/optimalButtonWidth);
        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                ScreenListingItem item = (ScreenListingItem)gridView.getAdapter().getItem(position);
            	Intent intent = new Intent(context,RActivity.class);  
            	intent.putExtra(ScreenListingItem.LAYOUT_ID,item.getLayoutId());
            	intent.putExtra(ScreenListingItem.FILTER_EXTRA,item.getExtraFilterClass());
            	context.startActivity(intent);
            }
        });
	}

}
