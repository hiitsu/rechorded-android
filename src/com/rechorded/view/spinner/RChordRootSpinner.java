package com.rechorded.view.spinner;

import java.util.Arrays;
import java.util.List;

import com.rechorded.activity.RActivity;
import com.rechorded.filter.RChordRootFilter;
import com.rechorded.filter.RChordTypeFilter;
import com.rechorded.filter.RDifficultyFilter;
import com.rechorded.model.Data;
import com.takakontti.AutoFitArrayAdapter;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class RChordRootSpinner extends Spinner implements OnItemSelectedListener {
	public RChordRootSpinner(Context context) {
		super(context);
		init(context);
	}

	public RChordRootSpinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public RChordRootSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		RActivity activity = (RActivity)context;
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line,Data.NOTES);
        Spinner spinner = this;
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
		int viewId = getId();
		activity.getFilters().put(viewId,new RChordRootFilter());
		setSelection(Arrays.asList(Data.NOTENUMS).indexOf(RChordRootFilter.DEFAULT_VALUE));
	}

	public void onItemSelected(AdapterView<?> spinner, View view, int position,long id) {
		String value = (String) getItemAtPosition(position);
		notifyListeners(value);
	}

	public void onNothingSelected(AdapterView<?> spinner) {
		//if( listener != null );
			// TODO disable filter?
			//listener.filter(spinner.getId(),);
	}
	private void notifyListeners(String root) {
		RActivity activity = (RActivity)getContext();
		activity.notifyFilterChanged(this.getId(), root);
	}
}
