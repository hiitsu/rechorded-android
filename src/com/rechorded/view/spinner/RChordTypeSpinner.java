package com.rechorded.view.spinner;

import java.util.List;

import com.rechorded.activity.RActivity;

import com.rechorded.filter.RChordTypeFilter;

import com.rechorded.model.Data;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.AdapterView.OnItemSelectedListener;

public class RChordTypeSpinner extends Spinner implements OnItemSelectedListener {
	public RChordTypeSpinner(Context context) {
		super(context);
		init(context);
	}

	public RChordTypeSpinner(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public RChordTypeSpinner(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(context);
		List<String> list = data.getDistinct(Data.TABLE_GUITARCHORD,RChordTypeFilter.TABLE_COLUMN);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, android.R.layout.simple_dropdown_item_1line,list);
        Spinner spinner = this;
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(this);
		int viewId = getId();
		activity.getFilters().put(viewId,new RChordTypeFilter());
		setSelection(list.indexOf(RChordTypeFilter.DEFAULT_VALUE));
	}

	public void onItemSelected(AdapterView<?> spinner, View view, int position,long id) {
		String value = (String) getItemAtPosition(position);
		notifyListeners(value);
	}

	public void onNothingSelected(AdapterView<?> spinner) {
		//if( listener != null );
			// TODO disable filter?
			//listener.filter(spinner.getId(),);
	}
	private void notifyListeners(String root) {
		RActivity activity = (RActivity)getContext();
		activity.notifyFilterChanged(this.getId(), root);
	}
}
