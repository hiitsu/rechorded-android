package com.rechorded.view.seekbar;

import com.rechorded.R;
import com.rechorded.activity.RActivity;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;

import android.util.AttributeSet;
import android.util.Log;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;

public class RSeekBar extends SeekBar implements OnSeekBarChangeListener {
	private static final String TAG = "RSeekBar";
	Drawable thumb;
	
	public RSeekBar(Context context) {
		super(context);
		init(context);
	}

	public RSeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public RSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void init(Context context) {
		setOnSeekBarChangeListener(this);
	}
	@Override
	protected void onFinishInflate() {
	    super.onFinishInflate();
	    thumb = getContext().getResources().getDrawable(R.drawable.seekbarhandle);
	    setThumb( thumb );
	}
	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w,h, oldw, oldh);
		if( thumb != null ) {
			thumb.setBounds(0,0,thumb.getIntrinsicHeight(),h);
		    int progress = getProgress();
		    setProgress(0);
		    setProgress(progress);
			
		}
		
	}

	public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
		RActivity activity = (RActivity)getContext();
		activity.notifyFilterChanged(getId(), String.valueOf(progress));
	}

	public void onStartTrackingTouch(SeekBar seekBar) {
		Log.d(TAG,"onStartTrackingTouch");
	}

	public void onStopTrackingTouch(SeekBar seekBar) {
		Log.d(TAG,"onStopTrackingTouch");
	}

}

