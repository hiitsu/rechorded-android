package com.rechorded.view.seekbar;

import java.util.List;

import com.rechorded.activity.RActivity;
import com.rechorded.filter.RDifficultyFilter;

import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class RDifficultySeekBar extends RSeekBar {

	public RDifficultySeekBar(Context context) {
		super(context);
		init(context);
	}

	public RDifficultySeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public RDifficultySeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	
	private void init(Context context) {
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(activity);
		List<String> list = data.getDistinct(Data.TABLE_GUITARCHORD,
				RDifficultyFilter.TABLE_COLUMN);
		//Collections.sort(list);
		int viewId = getId();
		activity.getFilters().put(viewId,new RDifficultyFilter());
		int max = Integer.valueOf(list.get(list.size()-1));
		setMax(max);
		setProgress(Integer.valueOf(RDifficultyFilter.DEFAULT_VALUE));
	}
}
