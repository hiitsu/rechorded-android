package com.rechorded.view.seekbar;

import java.util.List;

import com.rechorded.activity.RActivity;
import com.rechorded.filter.RPianoNoteCountFilter;

import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class RPianoFingerUsageSeekBar extends RSeekBar {

	public RPianoFingerUsageSeekBar(Context context) {
		super(context);
		init(context);
	}

	public RPianoFingerUsageSeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public RPianoFingerUsageSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}
	private void init(Context context) {
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(activity);
		List<String> list = data.getPianoChordFingerUsages();
		//Collections.sort(list);
		int viewId = getId();
		activity.getFilters().put(viewId,new RPianoNoteCountFilter());
		int max = Integer.valueOf(list.get(list.size()-1));
		setMax(max);
		setProgress(Integer.valueOf(RPianoNoteCountFilter.DEFAULT_VALUE));
	}
}
