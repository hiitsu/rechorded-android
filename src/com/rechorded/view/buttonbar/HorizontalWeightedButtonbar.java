package com.rechorded.view.buttonbar;

import java.util.ArrayList;

import com.rechorded.R;
import com.rechorded.view.button.RButton;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.view.View;
import android.view.View.OnClickListener;


public abstract class HorizontalWeightedButtonbar extends LinearLayout implements OnClickListener {
	ArrayList<RButton> buttons = new ArrayList<RButton>();
	//int minimumWidth = 60;
	int passiveDrawable = R.drawable.purplebutton;
	
	public HorizontalWeightedButtonbar(Context context) {
		super(context);
	}

	public HorizontalWeightedButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	protected void initButtons(Context context,String[] texts) {
		setOrientation(LinearLayout.HORIZONTAL);
		setWeightSum(texts.length);
		for(int i = 0; i < texts.length; i++) {
			RButton button = new RButton(context);
			button.setOnClickListener(this);
			button.setBackgroundResource(passiveDrawable);
			button.setText(texts[i]);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(0,LayoutParams.MATCH_PARENT,1);
			addView(button,params);
			buttons.add(button);
		}
	}
    @Override
    protected void onSizeChanged (int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        /*if (h != oldh) {
            for(Button b : buttons) {
            	b.setTextSize(h/3*2);
            }
        }*/
    }
    
	@Override
	public void onClick(View view) {
		RButton button = (RButton)view;
		clicked(button.getText());
		select(button);
	}
	public void select(RButton selectedButton) {
		for(RButton b : buttons) {
			b.setBackgroundResource(passiveDrawable);
		}
		selectedButton.setBackgroundResource(R.drawable.redbutton);
	}
	public void select(int index) {
		select(buttons.get(index));
	}
	public abstract void clicked(CharSequence text);
}
