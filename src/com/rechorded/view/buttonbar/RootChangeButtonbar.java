package com.rechorded.view.buttonbar;

import java.util.Arrays;

import com.rechorded.activity.RActivity;
import com.rechorded.filter.RChordRootFilter;
import com.rechorded.filter.RDifficultyFilter;
import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class RootChangeButtonbar extends HorizontalScrollingButtonbar {
	
	public RootChangeButtonbar(Context context) {
		super(context);
		initPrivate(context);
	}

	public RootChangeButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPrivate(context);
	}

	private void initPrivate(Context context) {
		super.initButtons(context,Data.NOTES);
		RActivity activity = (RActivity)context;
		super.fakeClick(0);
	}
	
	public void clicked(CharSequence s) {
		RActivity activity = (RActivity)getContext();
		int index = Arrays.asList(Data.NOTES).indexOf(s);
		int index2 = Arrays.binarySearch(Data.NOTES,s.toString());
		activity.notifyRootChanged(index);
	}

}
