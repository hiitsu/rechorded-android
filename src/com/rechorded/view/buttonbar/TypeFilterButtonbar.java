package com.rechorded.view.buttonbar;

import java.util.List;

import com.rechorded.activity.RActivity;

import com.rechorded.filter.RChordTypeFilter;
import com.rechorded.filter.RFingerCountFilter;
import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class TypeFilterButtonbar extends HorizontalScrollingButtonbar {
	List<String> values;
	
	public TypeFilterButtonbar(Context context) {
		super(context);
		initPrivate(context);
	}

	public TypeFilterButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPrivate(context);
	}

	private void initPrivate(Context context) {
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(activity);
		values = data.getDistinct(Data.TABLE_PIANOCHORD,"type");
		super.initButtons(context,values.toArray(new String[0]));
		activity.getFilters().put(getId(),new RChordTypeFilter());
		int n = values.indexOf(RChordTypeFilter.DEFAULT_VALUE);
		super.fakeClick(n);
	}
	
	public void clicked(CharSequence s) {
		RActivity activity = (RActivity)getContext();
		activity.notifyFilterChanged(getId(),s.toString());
	}

}
