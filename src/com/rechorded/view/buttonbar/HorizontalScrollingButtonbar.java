package com.rechorded.view.buttonbar;

import java.util.ArrayList;
import java.util.HashMap;

import com.rechorded.R;
import com.rechorded.view.button.RButton;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.view.View;
import android.view.View.OnClickListener;


public abstract class HorizontalScrollingButtonbar extends HorizontalScrollView implements OnClickListener {
	ArrayList<RButton> buttons = new ArrayList<RButton>();
	int minimumWidth = 60;
	int passiveDrawable = R.drawable.purplebutton;
	int selectedIndex = -1;

	public HorizontalScrollingButtonbar(Context context) {
		super(context);
	}

	public int getPassiveDrawable() {
		return passiveDrawable;
	}

	public void setPassiveDrawable(int passiveDrawable) {
		this.passiveDrawable = passiveDrawable;
	}

	public HorizontalScrollingButtonbar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public HorizontalScrollingButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	protected void initButtons(Context context,String[] values) {
		LinearLayout layout = new LinearLayout(context);
		layout.setOrientation(LinearLayout.HORIZONTAL);
		for(int i = 0; i < values.length; i++) {
			RButton button = new RButton(context);
			button.setMinimumWidth(minimumWidth);
			button.setOnClickListener(this);
			button.setBackgroundResource(passiveDrawable);
			button.setText(values[i]);
			LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.MATCH_PARENT);
			layout.addView(button,params);
			buttons.add(button);
		}
		addView(layout,0);
	}

	@Override
	public void onClick(View view) {
		RButton button = (RButton)view;
		selectedIndex = buttons.indexOf(button);
		for(RButton b : buttons) {
			b.setBackgroundResource(passiveDrawable);
		}
		button.setBackgroundResource(R.drawable.redbutton);
		clicked(button.getText());
	}
	
	public void fakeClick(int index){
		onClick(buttons.get(index));
	}
	
	public abstract void clicked(CharSequence text);
	
	protected void onSizeChanged (int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		if( w != 0 && h != 0 && selectedIndex != -1 ) {
			this.post(new Runnable() {
				@Override
				public void run() {
					final int left = buttons.get(selectedIndex).getLeft();
					scrollTo(left,0);
				} 
			});			
		}
	}
	@Override
	public Parcelable onSaveInstanceState() {
		Bundle bundle = new Bundle();
		bundle.putParcelable("instanceState", super.onSaveInstanceState());
		bundle.putInt("selectedIndex",selectedIndex);
		return bundle;
	}

	@Override
	public void onRestoreInstanceState(Parcelable state) {
		if (state instanceof Bundle) {
			Bundle bundle = (Bundle) state;
			selectedIndex = bundle.getInt("selectedIndex");
			fakeClick(selectedIndex);
			super.onRestoreInstanceState(bundle.getParcelable("instanceState"));
			return;
		}
		super.onRestoreInstanceState(state);
	}
}
