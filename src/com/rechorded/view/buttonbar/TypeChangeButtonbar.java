package com.rechorded.view.buttonbar;

import java.util.List;

import com.rechorded.R;
import com.rechorded.activity.RActivity;

import com.rechorded.filter.RChordTypeFilter;
import com.rechorded.filter.RFingerCountFilter;
import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class TypeChangeButtonbar extends HorizontalScrollingButtonbar {
	List<String> values;
	
	public TypeChangeButtonbar(Context context) {
		super(context);
		initPrivate(context);
	}

	public TypeChangeButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPrivate(context);
	}

	private void initPrivate(Context context) {
		setPassiveDrawable(R.drawable.bluebutton);
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(activity);
		values = data.getDistinct(Data.TABLE_PIANOCHORD,"type");
		super.initButtons(context,values.toArray(new String[0]));
		super.fakeClick(0);
	}
	
	public void clicked(CharSequence s) {
		RActivity activity = (RActivity)getContext();
		activity.notifyTypeChanged(s.toString());
	}

}
