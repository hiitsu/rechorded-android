package com.rechorded.view.buttonbar;

import java.util.ArrayList;

import java.util.List;

import com.rechorded.activity.RActivity;

import com.rechorded.filter.RDifficultyFilter;
import com.rechorded.filter.RRangeFilter;
import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class DifficultyButtonbar extends HorizontalScrollingButtonbar {
	final List<String> texts = new ArrayList<String>();
	List<Integer[]> values = new ArrayList<Integer[]>();
	
	public DifficultyButtonbar(Context context) {
		super(context);
		initPrivate(context);
	}

	public DifficultyButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPrivate(context);
	}

	private void initPrivate(Context context) {
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(activity);
		List<String> numbers = data.getDistinct(Data.TABLE_GUITARCHORD,RDifficultyFilter.TABLE_COLUMN);
		texts.add("Piece of cake");
		texts.add("Easy");
		texts.add("Intermediate");
		texts.add("Difficult");
		int m = Integer.valueOf(numbers.get(0));
		int n = Integer.valueOf(numbers.get(numbers.size()-1));
		int r = n-m;
		int d = r/4;
		values.add(new Integer[]{m,m+d});
		values.add(new Integer[]{m+d,m+d+d});
		values.add(new Integer[]{m+d+d,m+d+d+d});
		values.add(new Integer[]{m+d+d+d,n});
		super.initButtons(context,texts.toArray(new String[0]));
		activity.getFilters().put(getId(),new RRangeFilter(Data.TABLE_GUITARCHORD,"difficulty",m,m+d));
		super.fakeClick(0);
	}
	
	public void clicked(CharSequence s) {
		RActivity activity = (RActivity)getContext();
		Object minmax = values.get(texts.indexOf(s));
		activity.notifyFilterChanged(getId(),minmax );
	}

}
