package com.rechorded.view.buttonbar;

import java.util.List;

import com.rechorded.activity.RActivity;

import com.rechorded.filter.RFingerCountFilter;
import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class FingerCountButtonbar extends HorizontalScrollingButtonbar {
	List<String> values;
	
	public FingerCountButtonbar(Context context) {
		super(context);
		initPrivate(context);
	}

	public FingerCountButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPrivate(context);
	}

	private void initPrivate(Context context) {
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(activity);
		values = data.getDistinct(Data.TABLE_GUITARCHORD,"count");
		super.initButtons(context,values.toArray(new String[0]));
		activity.getFilters().put(getId(),new RFingerCountFilter());
		int n = values.indexOf(RFingerCountFilter.DEFAULT_VALUE);
		super.fakeClick(n);
	}
	
	public void clicked(CharSequence s) {
		RActivity activity = (RActivity)getContext();
		activity.notifyFilterChanged(getId(),s.toString());
	}

}
