package com.rechorded.view.buttonbar;

import java.util.Arrays;

import com.rechorded.activity.RActivity;
import com.rechorded.filter.RChordRootFilter;
import com.rechorded.filter.RDifficultyFilter;
import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class RootFilterButtonbar extends HorizontalScrollingButtonbar {
	
	public RootFilterButtonbar(Context context) {
		super(context);
		initPrivate(context);
	}

	public RootFilterButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPrivate(context);
	}

	private void initPrivate(Context context) {
		super.initButtons(context,Data.NOTES);
		RActivity activity = (RActivity)context;
		activity.getFilters().put(getId(),new RChordRootFilter());
		int n = Arrays.asList(Data.NOTENUMS).indexOf(RChordRootFilter.DEFAULT_VALUE);
		super.fakeClick(n);
	}
	
	public void clicked(CharSequence s) {
		for(int i=0; i < Data.NOTES.length;i++) {
			if( Data.NOTES[i].equals(s) ) {
				RActivity activity = (RActivity)getContext();
				activity.notifyFilterChanged(getId(),Data.NOTENUMS[i]);
			}
		}
	}

}
