package com.rechorded.view.buttonbar;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.rechorded.activity.RActivity;
import com.rechorded.filter.RBarreFilter;
import com.rechorded.filter.RDifficultyFilter;
import com.rechorded.filter.RFingerCountFilter;
import com.rechorded.filter.RPianoNoteCountFilter;
import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class NoteCountButtonbar extends HorizontalScrollingButtonbar {
	final List<String> texts = new ArrayList<String>();
	final List<String> values = new ArrayList<String>();
	
	public NoteCountButtonbar(Context context) {
		super(context);
		initPrivate(context);
	}

	public NoteCountButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPrivate(context);
	}

	private void initPrivate(Context context) {
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(activity);
		List<String> numbers = data.getPianoChordFingerUsages();
		for(String n:numbers) {
			texts.add(n+" notes");
			values.add(n);
		}
		super.initButtons(context,texts.toArray(new String[0]));
		activity.getFilters().put(getId(),new RPianoNoteCountFilter());
		int selectedIndex = values.indexOf(RPianoNoteCountFilter.DEFAULT_VALUE);
		super.fakeClick(selectedIndex);
	}
	
	public void clicked(CharSequence s) {
		RActivity activity = (RActivity)getContext();
		activity.notifyFilterChanged(getId(), values.get(texts.indexOf(s)));
	}

}
