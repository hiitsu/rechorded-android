package com.rechorded.view.buttonbar;

import java.util.ArrayList;

import java.util.List;

import com.rechorded.activity.RActivity;
import com.rechorded.filter.RBarreFilter;

import com.rechorded.model.Data;

import android.content.Context;
import android.util.AttributeSet;

public class BarreButtonbar extends HorizontalScrollingButtonbar {
	final List<String> texts = new ArrayList<String>();
	final List<String> values = new ArrayList<String>();
	
	public BarreButtonbar(Context context) {
		super(context);
		initPrivate(context);
	}

	public BarreButtonbar(Context context, AttributeSet attrs) {
		super(context, attrs);
		initPrivate(context);
	}

	private void initPrivate(Context context) {
		RActivity activity = (RActivity)context;
		Data data = Data.getInstance(activity);
		long max = data.getMax(Data.TABLE_GUITARCHORD,RBarreFilter.TABLE_COLUMN);
		texts.add("No barre");
		values.add(""+0);
		for(int i=1; i<=max; i++) {
			texts.add(i+".");
			values.add(""+i);
		}
		super.initButtons(context,texts.toArray(new String[0]));
		activity.getFilters().put(getId(),new RBarreFilter());
		int n = values.indexOf(RBarreFilter.DEFAULT_VALUE);
		super.fakeClick(n);
	}
	
	public void clicked(CharSequence s) {
		RActivity activity = (RActivity)getContext();
		activity.notifyFilterChanged(getId(), values.get(texts.indexOf(s)));
	}

}
