package com.rechorded.view;

import android.os.AsyncTask;
import android.util.Log;

public class RPainterTimer extends AsyncTask<Long, Void, Void> {
	
	private static final String TAG = RPainterTimer.class.getSimpleName();
	RPainter painter;
	
    public RPainterTimer(RPainter painter) {
		super();
		this.painter = painter;
	}
    @SuppressWarnings("unused")
	private RPainterTimer() {}
    @Override
    protected void onPreExecute() {
    }

    @Override
    protected void onProgressUpdate(Void... values) {
    }

	@Override
	protected Void doInBackground(Long... params) {
		long waitingMillis = params[0];
		long now = System.currentTimeMillis();
		long startedMillis = now;
		while( now-startedMillis < waitingMillis ) {
	        try {
	            Thread.sleep(200);
	        } catch (InterruptedException e) {
	            Log.w(TAG,"interrupted");
	        }
	        now = System.currentTimeMillis();
		}
		return null;
	}

	@Override
	protected void onPostExecute(Void result) {
		super.onPostExecute(result);
		//painter.setOverlayText(null);
		//painter.clearTimer();
	}
	
} 