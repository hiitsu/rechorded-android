package com.rechorded.filter;

import com.rechorded.model.Data;

public class RPianoNoteCountFilter extends RAbstractFilter {
	final String Q = "SELECT type FROM (SELECT type,count(num) as n FROM chordstep GROUP BY type) WHERE n=:fingerCount";
	public static final String TABLE_COLUMN = "count",
			DEFAULT_VALUE = "3";
	String value = DEFAULT_VALUE;
	public RPianoNoteCountFilter() {
		super();
	}
	public String whereClause() {
		String s = "type IN ("+Q.replace(":fingerCount",value)+")";
		return s;
	}
	public void setValue(Object value) {
		this.value = (String) value;
	}
	public Object getValue() {
		return value;
	}
	public String table() {
		return Data.TABLE_PIANOCHORD;
	}
}
