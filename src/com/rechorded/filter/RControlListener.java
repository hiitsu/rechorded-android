package com.rechorded.filter;

public interface RControlListener {
	public void filter(int filterId, String value);
	public void select(String value);
}
