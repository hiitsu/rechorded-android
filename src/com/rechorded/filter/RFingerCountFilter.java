package com.rechorded.filter;

import com.rechorded.model.Data;

public class RFingerCountFilter extends RSimpleFilter {
	public static final String TABLE_COLUMN = "count",DEFAULT_VALUE = "1";
	public RFingerCountFilter() {
		super(Data.TABLE_GUITARCHORD, TABLE_COLUMN,DEFAULT_VALUE,false);
	}
	
}

