package com.rechorded.filter;

public class RRangeFilter extends RAbstractFilter {
	
	String table,column;
	Integer[] minmax;

	public RRangeFilter(String table, String column,int min, int max) {
		super();
		this.table = table;
		this.column = column;
	}

	public String table() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}
	
	public Object getValue() {
		return minmax;
	}

	public void setValue(Object value) {
		this.minmax = (Integer[]) value;
	}

	public String whereClause() {
		String base = ":column>=:min AND :column<:max";
		String s = RAbstractFilter.replace(base,":column",column,false);
		s = RAbstractFilter.replace(s,":min",""+minmax[0],false);
		s = RAbstractFilter.replace(s,":max",""+minmax[1],false);
		return s;
	}

}

