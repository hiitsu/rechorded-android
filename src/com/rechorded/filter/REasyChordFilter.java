package com.rechorded.filter;

import com.rechorded.model.Data;

public class REasyChordFilter extends RAbstractFilter {

	public String whereClause() {
		return "count <= 3 AND difficulty <=5 AND barre=0 AND type IN ('maj','m','7','m7','maj7')";
	}
	public String table() {
		return Data.TABLE_GUITARCHORD;
	}
	public void setValue(Object value) {
	}

	public Object getValue() {
		return null;
	}

}
