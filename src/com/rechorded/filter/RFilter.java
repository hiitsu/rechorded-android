package com.rechorded.filter;

import java.util.HashMap;

/**
 * Base interface for filters.
 */
public interface RFilter {
	/**
	 * Generate the SQL  where clause without WHERE prepended.
	 * @return SQL String
	 */
	public String whereClause();
	public String table();
	/**
	 * Called when object changed filter value.
	 * @param value
	 */
	public void setValue(Object value);
	public Object getValue();
	
	public void setEnabled(boolean enabled);
	public boolean isEnabled();

}
