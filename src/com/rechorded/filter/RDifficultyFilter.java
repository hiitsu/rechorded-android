package com.rechorded.filter;

import com.rechorded.model.Data;

public class RDifficultyFilter extends RSimpleFilter {
	public static final String TABLE_COLUMN = "difficulty",DEFAULT_VALUE = "1";
	public RDifficultyFilter() {
		super(Data.TABLE_GUITARCHORD, TABLE_COLUMN,DEFAULT_VALUE,false);
	}
}
