package com.rechorded.filter;

import com.rechorded.model.Data;

public class RBarreFilter extends RSimpleFilter {
	
	public final static String 
				TABLE_COLUMN 	= "barre",
				DEFAULT_VALUE	= "0";
	
	public RBarreFilter() {
		super(Data.TABLE_GUITARCHORD, TABLE_COLUMN,DEFAULT_VALUE,false);
	}
}
