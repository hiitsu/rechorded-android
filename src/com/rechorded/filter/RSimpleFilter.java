package com.rechorded.filter;

public class RSimpleFilter extends RAbstractFilter {
	
	String table,column,value;
	boolean stringFlag = false;

	public RSimpleFilter(String table, String column, String value,
			boolean stringFlag) {
		super();
		this.table = table;
		this.column = column;
		this.value = value;
		this.stringFlag = stringFlag;
	}

	public String table() {
		return table;
	}

	public void setTable(String table) {
		this.table = table;
	}

	public String getColumn() {
		return column;
	}

	public void setColumn(String column) {
		this.column = column;
	}
	
	public Object getValue() {
		return value;
	}

	public void setValue(Object value) {
		this.value = (String) value;
	}

	public boolean isStringFlag() {
		return stringFlag;
	}

	public void setStringFlag(boolean stringFlag) {
		this.stringFlag = stringFlag;
	}

	public String whereClause() {
		String s = RAbstractFilter.replace(column+"=:value",":value",value,stringFlag);
		return s;
	}

}

