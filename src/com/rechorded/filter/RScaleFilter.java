package com.rechorded.filter;

import com.rechorded.model.Data;

public class RScaleFilter extends RAbstractFilter {
	final String Q = 
			"SELECT DISTINCT root,type FROM guitarchord a LEFT OUTER JOIN scale2chord b ON b.scaleroot=0 AND b.scaletype='ionian' AND a.root=b.chordroot AND a.type=b.chordtype;";
	public static final String TABLE_COLUMN = "count",
			DEFAULT_VALUE = "3";
	String value = DEFAULT_VALUE;
	public RScaleFilter() {
		super();
	}
	public String whereClause() {
		return "type IN ("+Q.replace(":fingerCount",value)+")";
	}
	public void setValue(Object value) {
		this.value = (String) value;
	}
	public Object getValue() {
		return value;
	}
	public String table() {
		return Data.TABLE_PIANOCHORD;
	}
}