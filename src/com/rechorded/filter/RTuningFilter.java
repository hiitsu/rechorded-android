package com.rechorded.filter;

import com.rechorded.model.Data;

public class RTuningFilter extends RSimpleFilter {
	public static final String TABLE_COLUMN = "tuning",DEFAULT_VALUE=Data.DEFAULT_TUNING;
	public RTuningFilter() {
		super(Data.TABLE_GUITARCHORD,TABLE_COLUMN,DEFAULT_VALUE,true);
	}
	public RTuningFilter(String tuning) {
		super(Data.TABLE_GUITARCHORD,TABLE_COLUMN,tuning,true);
	}
}
