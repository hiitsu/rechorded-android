package com.rechorded.filter;

import com.rechorded.model.Data;

public class RChordTypeFilter extends RSimpleFilter {
	public static final String TABLE_COLUMN = "type",DEFAULT_VALUE="maj";
	public RChordTypeFilter() {
		super(Data.TABLE_GUITARCHORD,TABLE_COLUMN,DEFAULT_VALUE,true);
	}
	public RChordTypeFilter(String type) {
		super(Data.TABLE_GUITARCHORD,TABLE_COLUMN,type,true);
	}
}
