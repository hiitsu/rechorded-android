package com.rechorded.filter;

import java.util.Collection;

import com.rechorded.model.Data;

public abstract class RAbstractFilter implements RFilter {
	int id;
	boolean enabled = true;
	
	public RAbstractFilter() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public static String replace(String s,String needle,String value,boolean isString){
		if( isString ) {
			//return s.replace(needle,DatabaseUtils.sqlEscapeString(value));
			return s.replace(needle,"'"+value+"'");
		}
		return s.replace(needle, value);
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	static public boolean containsGuitarOnlyFilters(Collection<RFilter> filters) {
		for( RFilter f : filters )
			if( f.table().equals(Data.TABLE_GUITARCHORD) )
				return true;
		return false;
	}
}
