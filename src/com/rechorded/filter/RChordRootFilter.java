package com.rechorded.filter;

import java.util.Arrays;

import com.rechorded.model.Data;

public class RChordRootFilter extends RSimpleFilter {
	
	public final static String TABLE_COLUMN = "root";
	public static final String DEFAULT_VALUE="0";
	public RChordRootFilter() {
		super(Data.TABLE_GUITARCHORD,TABLE_COLUMN,DEFAULT_VALUE,false);
	}

	public RChordRootFilter(String root) {
		super(Data.TABLE_GUITARCHORD,TABLE_COLUMN,DEFAULT_VALUE,false);
		setValue(root);
	}

	public void setValue(String value) {
		int pos = Arrays.asList(Data.NOTES).indexOf(value);
		if( pos == -1 )
			this.value = value;
		else
			this.value = String.valueOf(pos);
	}
	
}
