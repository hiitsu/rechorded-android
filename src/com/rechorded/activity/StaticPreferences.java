package com.rechorded.activity;

import com.rechorded.model.Data;

public class StaticPreferences {
	public static boolean sound = true;
	public static int delay = 150;
	public static String tuning = Data.DEFAULT_TUNING;
}
