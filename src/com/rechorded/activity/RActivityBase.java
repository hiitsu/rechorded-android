package com.rechorded.activity;

import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.TypedValue;

public class RActivityBase extends RMenuHelperActivity {
	int optimalButtonWidthDP = 70, 
		optimalButtonHeightDP = 70,
		optimalButtonWidth = optimalButtonWidthDP,
		optimalButtonHeight = optimalButtonHeightDP;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		optimalButtonWidth = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,optimalButtonWidthDP,metrics));
		optimalButtonHeight = Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,optimalButtonWidthDP,metrics));
	}

	public int getOptimalWidth() {
		return optimalButtonWidth;
	}

	public int getOptimalHeight() {
		return optimalButtonHeight;
	}
	public int calculateColumnCount(){
		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);
		int v = metrics.widthPixels / optimalButtonWidth;
		return v;
	}
	
}
