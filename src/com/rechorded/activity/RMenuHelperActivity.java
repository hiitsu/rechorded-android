package com.rechorded.activity;

import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;

import com.rechorded.R;

public class RMenuHelperActivity extends Activity {
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.common, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	int id = item.getItemId();
        switch(id) {
        	case R.id.menu_preferences:
        		startActivity(new Intent(this, PreferenceEditingActivity.class));
        		return true;
        	default:
        		break;
        }
        return false;
    }
}
