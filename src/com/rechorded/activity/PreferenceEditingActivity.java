package com.rechorded.activity;

import java.util.List;

import com.rechorded.R;
import com.rechorded.model.Data;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceManager;
import android.util.Log;
 
public class PreferenceEditingActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener {
	final static String TAG = PreferenceEditingActivity.class.getSimpleName();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.preferences);

        ListPreference tuningPreference = (ListPreference) getPreferenceManager().findPreference("tuning");
       	List<String> entries = Data.getInstance(this).getDistinct("tuning","name");
       	tuningPreference.setEntries(entries.toArray(new String[0]));
       	tuningPreference.setEntryValues(entries.toArray(new String[0]));
       	tuningPreference.setValue(StaticPreferences.tuning);
       	tuningPreference.setSummary(tuningPreference.getValue());
    }
    
    
    @Override
	protected void onPause() {
		super.onPause();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
       	prefs.unregisterOnSharedPreferenceChangeListener(this);
	}


	@Override
	protected void onResume() {
		super.onResume();
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
       	prefs.registerOnSharedPreferenceChangeListener(this);
	}


	@Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
     	if( key.equals("sound") ) {
     		StaticPreferences.sound = sharedPreferences.getBoolean("sound",true);
     		Log.i(TAG,key+" is now "+StaticPreferences.sound);
     	} else if( key.equals("delay") ) {
     		StaticPreferences.delay = sharedPreferences.getInt("delay",150);
     		Log.i(TAG,key+" is now "+StaticPreferences.delay);
     	} else if( key.equals("tuning") ) {
     		StaticPreferences.tuning = sharedPreferences.getString("tuning",Data.DEFAULT_TUNING);
     		ListPreference tuningPreference = (ListPreference) getPreferenceManager().findPreference("tuning");
     		tuningPreference.setSummary(StaticPreferences.tuning);
     		Log.i(TAG,key+" is now "+StaticPreferences.tuning);
     	}
    }
}