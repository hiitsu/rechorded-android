package com.rechorded.activity;

import java.io.File;

import com.rechorded.R;
import com.rechorded.model.Data;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.TextView;

public class LoadingActivity extends Activity {
	final static private String TAG = "LoadingActivity";
	protected TextView percentField;
	protected InitTask initTask;
    
    @Override
    public void onCreate( Bundle savedInstanceState ) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.loadingscreen );
        percentField = ( TextView ) findViewById( R.id.percentField );
		Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/webissimo-regular.ttf");
		percentField.setTypeface(typeface);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
 		StaticPreferences.sound = prefs.getBoolean("sound",true);
     	Log.i(TAG,"Preferences.sound is now "+StaticPreferences.sound);
 		StaticPreferences.delay = prefs.getInt("delay",150);
 		Log.i(TAG,"Preferences.delay is now "+StaticPreferences.delay);
 		StaticPreferences.tuning = prefs.getString("tuning",Data.DEFAULT_TUNING);
		initTask = new InitTask();
        initTask.execute( this );
    }
    
    protected class InitTask extends AsyncTask<Context, Integer, String>{

		@Override
		protected String doInBackground( Context... params ) {

			Data.getInstance(getBaseContext());
			publishProgress( 50 );
			SoundManager.getInstance(getBaseContext());
			publishProgress( 100 );
			for(int i=0; i <= 10; i++)
			try{
				Thread.sleep(100);
			} catch( Exception e ){
				Log.i(TAG, e.getMessage() );
			}
			return "COMPLETED!";
		}
		

		@Override
		protected void onPreExecute() 
		{
			Log.i(TAG, "onPreExecute()" );
			super.onPreExecute();
			
		}
		

		@Override
		protected void onProgressUpdate(Integer... values) 
		{
			super.onProgressUpdate(values);
			//Log.i(TAG, "onProgressUpdate(): " +  String.valueOf( values[0] ) );
			percentField.setText( values[0] + "%");
		}
		

		@Override
		protected void onCancelled()
		{
			super.onCancelled();
			Log.i(TAG, "onCancelled()" );
			percentField.setText( "Cancelled!" );
		}


		@Override
		protected void onPostExecute( String result ) 
		{
			super.onPostExecute(result);
			Log.i(TAG, "onPostExecute(): " + result );
	        Intent intent = new Intent(getBaseContext(), SelectionScreenActivity.class);
	        startActivity(intent);
		}
    }    
}