package com.rechorded.activity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.rechorded.filter.RAbstractFilter;
import com.rechorded.filter.RFilter;
import com.rechorded.model.Chord;
import com.rechorded.model.Data;
import com.rechorded.model.RFilterChangeListener;
import com.rechorded.model.RSelectionChangeListener;
import com.rechorded.view.gridview.ScreenListingItem;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;



@SuppressLint("UseSparseArrays")
public class RActivity extends RActivityBase {
	
	private static final String	TAG = RActivity.class.getSimpleName();
	
	ArrayList<RFilterChangeListener> filterChangeListeners = new ArrayList<RFilterChangeListener>();
	ArrayList<RSelectionChangeListener> selectionChangeListeners = new ArrayList<RSelectionChangeListener>();
	MetronomeTask metronomeTask;
	HashMap<Integer,RFilter> filters = new HashMap<Integer,RFilter>();

	@Override
    public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		Intent intent = getIntent();
		int layoutId = intent.getIntExtra(ScreenListingItem.LAYOUT_ID,-1);
		if( layoutId != -1 ){
			setContentView(layoutId);
		}
		String filterClassName = intent.getStringExtra(ScreenListingItem.FILTER_EXTRA);
		if( filterClassName != null ){
			RFilter filter = null;
			try {
				filter = (RFilter) Class.forName("com.rechorded.filter."+filterClassName).newInstance();
				filters.put(UUID.randomUUID().hashCode(),filter);
			} catch (Exception e) {
				Log.e(TAG,"Filter not found",e);
			} 
		}
	}
	@Override
	protected void onResume() {
		super.onResume();
		notifyFilterChanged(View.NO_ID,"");		
	}    
	
	@Override
	public void onPause() {
	    super.onPause();
	    SoundManager.getInstance(this).onPause();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
	}
	
	public HashMap<Integer, RFilter> getFilters() {
		return filters;
	}
	public void notifyRootChanged(int root) {
		for(RSelectionChangeListener listener:selectionChangeListeners){
			listener.changeChordRoot(root);
		}	
	}
	public void notifySelectionChanged(Chord chord) {
		playChord(chord);
		for(RSelectionChangeListener listener:selectionChangeListeners){
			listener.selectionChanged(chord);
		}	
	}
	public void notifyTypeChanged(String type) {
		for(RSelectionChangeListener listener:selectionChangeListeners){
			listener.changeChordType(type);
		}	
	}
	public void notifyFilterChanged(int filterId,Object value){
		Data data = Data.getInstance(this);
		if( !filters.containsKey(filterId) ) {
			Log.w(TAG,"No filter by id: "+filterId);
		} else if( filterId != View.NO_ID ){
			RFilter filter = filters.get(filterId);
			filter.setValue(value);
		}
		
		Collection<RFilter> values = filters.values();
		boolean hasGuitarOnlyFilters = RAbstractFilter.containsGuitarOnlyFilters(values);
		Cursor cursor = null;
		if( hasGuitarOnlyFilters )
			cursor = data.getGroupedGuitarChordCursor(values);
		else
			cursor = data.getPianoChords(values);
		for(RFilterChangeListener listener:filterChangeListeners){
			listener.filterChanged(filterId, value, cursor);
		}
	}
	public void registerSelectionChangeListener(RSelectionChangeListener listener){
		selectionChangeListeners.add(listener);
	}
	public void registerFilterChangeListener(RFilterChangeListener listener){
		filterChangeListeners.add(listener);
	}

	public void playChord(Chord chord){
		if( !StaticPreferences.sound )
			return;
		List<String> steps = Data.getInstance(this).getChordTypeSteps(chord.getType());
		int rootNum = chord.getRoot();
		SoundManager soundManager = SoundManager.getInstance(this);
		long d = StaticPreferences.delay;
		for(int i=0; i<steps.size();i++) {
			int n = Integer.valueOf(steps.get(i));
			int note = rootNum+n;
			if( i == 0 )
				soundManager.playNote(note,i*d);
			else
				soundManager.playNote(12+note,i*d);
		}
	}
}

/*public static void applyFont(final Context context, final View root, Typeface typeface) {
try {
    if (root instanceof ViewGroup) {
        ViewGroup viewGroup = (ViewGroup) root;
        for (int i = 0; i < viewGroup.getChildCount(); i++)
            RActivity.applyFont(context, viewGroup.getChildAt(i), typeface);
    } else if (root instanceof TextView) {
        ((TextView) root).setTypeface(typeface);
    }
} catch (Exception e) {
    Log.e(TAG, String.format("Error occured when trying to apply %s font for %s view", typeface.toString(), root));
    e.printStackTrace();
}
}*/

/*
 * 	Object[][] controlConfig = {
		{	R.id.fingerCountSlider,	RFingerCountFilter.class,	"slide to change finger count"		},
		{	R.id.difficultySlider,	RDifficultyFilter.class, 	"slide to change chord difficulty"	},
		{	R.id.barreSlider,		RBarreFilter.class, 		"slide to change barre"	},
		{	R.id.chordRootSpinner,	RChordRootFilter.class 	},
		{	R.id.chordTypeSpinner,	RChordTypeFilter.class 	},
		{	R.id.difficultySpinner,	RDifficultyFilter.class },
		{	R.id.barreSpinner,		RBarreFilter.class		},
		{	R.id.fingerCountSpinner,RFingerCountFilter.class},
		{	R.id.chordGridView,		null					}
	};
	int[][] labelConfig = {
		{	R.id.difficultySlider, 	R.id.difficultyValue 	},
		{	R.id.fingerCountSlider, R.id.fingerCountValue 	},
		{	R.id.barreSlider, 		R.id.barreValue 		}
	};
 * 		RWebView guitarView = (RWebView)findViewById(R.id.guitarView);
		if( guitarView != null ) instrumentsViews.add(guitarView);
		RWebView pianoView = (RWebView)findViewById(R.id.pianoView);
		if( pianoView != null ) instrumentsViews.add(pianoView);
		
		try {
			initComponents();
		} catch (Exception e) {
			Log.e(TAG,"error with components",e);
		}
		filter(-1,null);
 * 	public void filter(int filterId, String value) {
		if( filterId != -1 ) {
			RFilter filter = filters.get(filterId);
			if( filter == null ) {
				Log.w(TAG,"no filter by"+filterId);
				return;
			}
			String oldValue = filter.getValue();
			filter.setValue(value);
			if( oldValue.equals(value) ) {
				Log.w(TAG,"old value is same as new value:"+value);
				return;
			}
			Cursor cursor = Data.getInstance(this).getGuitarChordCursor(filters.values());
			for(RControl control: controls) {
				control.changed(cursor);
			}
			for(int[] a: labelConfig) {
				if( filterId == a[0] ){
					TextView textView = (TextView)findViewById(a[1]);
					if( textView != null )
						textView.setText(value);					
				}
			}
		}
	}
 * 	public void select(String value) {
		String[] a = value.split(Data.VALUE_SEP);
		for( RWebView targetView : instrumentsViews)
			targetView.setChord(a[0],a[1],a[2],a[3]);
	}
 *	void initComponents() throws InstantiationException, IllegalAccessException {
		Data data = Data.getInstance(this);
		int optimalSize = super.getOptimalWidth();
		for(Object[] a : controlConfig) {
			int controlId = (Integer)a[0];
			View view = findViewById(controlId);
			RControl control = (RControl)view;
			if( control != null ) {
				control.init(data,this,optimalSize);
				controls.add(control);
				if( a.length > 1 && a[1] != null ) {
					@SuppressWarnings("unchecked")
					Class<RFilter> clazz = (Class<RFilter>)a[1];
					RFilter filter = clazz.newInstance();
					filter.init(data, control);
					filters.put(controlId,filter);
				}
			}
			if( view != null && a.length > 2 && a[2] != null ) {
				String toastText = (String)a[2];
                int[] xy= new int[2];
                view.getLocationOnScreen(xy);
                Toast toast = Toast.makeText(this,toastText, Toast.LENGTH_SHORT);
                toast.setGravity(Gravity.CENTER, xy[0], xy[1]);
                toast.show();
			}
		}
	}
 * 		gridView = (GridView)findViewById(R.id.gridView);
		if( gridView != null) {
			ChordListingAdapter adapter = new ChordListingAdapter(this,this);
			adapter.setItems(data.getGuitarChords(selectionManager));
			adapter.setOptimalButtonHeight(getOptimalHeight());
			adapter.setOptimalButtonWidth(getOptimalWidth());
			gridView.setAdapter(adapter);
		}
		RWebView guitarView = (RWebView)findViewById(R.id.guitarView);
		if( guitarView != null ) targetViews.add(guitarView);
		RWebView pianoView = (RWebView)findViewById(R.id.pianoView);
		if( pianoView != null ) targetViews.add(pianoView);
 * 		String[] a = value.split(Data.VALUE_SEP);
		for( RWebView targetView : targetViews)
			targetView.setChord(a[0],a[1],a[2],a[3]);		String[] a = value.split(Data.VALUE_SEP);
		for( RWebView targetView : targetViews)
			targetView.setChord(a[0],a[1],a[2],a[3]);
 * 	public void update() {
		Data data = Data.getInstance(this);
		ChordResultMap map = data.getGuitarChords(selectionManager);
		if( map.size() > 0 )
			select(map.values().iterator().next().get(0));;		
	}
String value = (String) spinner.getAdapter().getItem(position);
		for(Object[] pair : MAP) {
			int id = (Integer)pair[0];
			if( id == identifier ) {
				
			}
		}
 * 		for(Object[] pair : MAP) {
			// tag = (String) pair[0];
			//View contentView = findViewById(android.R.id.content);
	        Spinner spinner = (Spinner)findViewWithId(tag);
	        if( spinner != null ) {
	        	DataProvider provider = (DataProvider)pair[1];
		        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line,provider.derivedData(data));
		        spinner.setAdapter(adapter);
		        spinner.setOnItemSelectedListener(this);
	        }
		}
 * //final static HashMap<Integer,DataProvider> MAP = new HashMap<Integer,DataProvider>();
 * 	static {
		MAP.put(R.id.chordRootSpinner,new DataProvider(){
			public String[] strings(Data data) {
				ArrayList<String> list = new ArrayList<String>();
				list.add("NO ROOT");
				list.addAll(Arrays.asList(Data.NOTES));
				return list.toArray(new String[0]);
			}
			public void actionForString(String s,RSelectionManager selectionManager) {
				if( )
				selectionManager.setChordRoot(String.valueOf(b))
			}
		});
	};
 * 
 	public void categorize() {
		ViewGroup rootViewGroup = (ViewGroup)findViewById(android.R.id.content);
		int c = rootViewGroup.getChildCount();
		for (int i=0; i < c; i++){
			View view = (View)rootViewGroup.getChildAt(i);
			String tag = (String) view.getTag();
			int id = view.getId();
			if( (tag != null && tag.equalsIgnoreCase("targetView")) || 
					id == R.id.guitarView || 
					id == R.id.pianoView )
				targetViews.add((RWebView)view);
		}
	}
 * 
RWebView guitarView = (RWebView)findViewById(R.id.guitarView);
if( guitarView != null ) targetViews.add(guitarView);
RWebView pianoView = (RWebView)findViewById(R.id.pianoView);
if( pianoView != null ) targetViews.add(pianoView);*/
