package com.rechorded.activity;

import java.lang.reflect.Field;
import java.util.HashMap;

import com.rechorded.R;

import android.content.Context;

import android.media.AudioManager;
import android.media.SoundPool;
import android.media.SoundPool.OnLoadCompleteListener;
import android.os.Handler;
import android.util.Log;

public class SoundManager {
	final String TAG="SoundManager";
	SoundPool soundPool;
	HashMap<Integer, Integer> noteMap,metroMap;
	AudioManager audioManager;
	Context context;
	
	boolean isPlayingMutedHack = false;
	int mutedHackSoundId = -1;
	
	static SoundManager instance;
	static int creationCounter = 0;
	public static synchronized SoundManager getInstance(Context c){
		if( instance == null ) {
			instance = new SoundManager(c);
			creationCounter++;
			if( creationCounter > 1 ){
				throw new Error("SoundManager instance created for the second time");
			}
		}
		return instance;
	}
	
	private SoundManager(Context context) {
		this.context = context;
		soundPool = new SoundPool(24,AudioManager.STREAM_MUSIC,0); 
		noteMap = new HashMap<Integer, Integer>();
		metroMap = new HashMap<Integer, Integer>();
		audioManager = (AudioManager)context.getSystemService(Context.AUDIO_SERVICE);
		loadSounds();
		soundPool.setOnLoadCompleteListener(new OnLoadCompleteListener() {
			public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
				Log.w(TAG,"sampleId:"+sampleId+" - status (success=0):"+status);
			}
		});
	} 
	  
	public void addNote(int index,int soundResourceId) {
		noteMap.put(index, soundPool.load(context, soundResourceId, 1));
	}
	/*public void addSound(int index,AssetFileDescriptor descriptor,long len) {
		int soundId = soundPool.load(descriptor.getFileDescriptor(),0,len,1);
		noteMap.put(index,soundId);
	}*/
	public void playMetronomeHigh(){
		int streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		soundPool.play(metroMap.get(R.raw.metronome_hi), streamVolume, streamVolume, 1, 0, 1f);		
	}
	public void playMetronomeLow(){
		int streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		soundPool.play(metroMap.get(R.raw.metronome_low), streamVolume, streamVolume, 1, 0, 1f);		
	}
	public void playNote(int index) {
		if( !isPlayingMutedHack && noteMap.size() > 0) {
			mutedHackSoundId = noteMap.entrySet().iterator().next().getValue();
			soundPool.play(mutedHackSoundId, 0, 0, 1, -1, 1f);
			isPlayingMutedHack = true;
		}
		int streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		int soundId = noteMap.get(index);
		soundPool.play(soundId, streamVolume, streamVolume, 1, 0, 1f);
	}
	public void playNote(int index,long delay){
		int soundId = noteMap.get(index);
		playDelayed(soundId,delay);
	}
	public void playDelayed(final int soundId, final long delay) {
		final int streamVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
		new Handler().postDelayed(new Runnable() {
			@Override
			public void run() {
				soundPool.play(soundId, streamVolume, streamVolume,1, 0, 1f); 
			}
		}, delay);
	}

	public void onPause() {
		if( isPlayingMutedHack ) {
			soundPool.stop(mutedHackSoundId);
			isPlayingMutedHack = false;
		}
	}
	public void dispose() {
		noteMap.clear();
		noteMap = null;
		metroMap.clear();
		metroMap = null;
		soundPool.release();
		soundPool = null;
		audioManager = null;
	}
	private void loadSounds() {
		metroMap.put(R.raw.metronome_low, soundPool.load(context, R.raw.metronome_low, 1));
		metroMap.put(R.raw.metronome_hi, soundPool.load(context, R.raw.metronome_hi, 1));
		for(int i=0; i < 48; i++) {
			String fieldName = "n"+String.valueOf(i);
			try {
				Field field = R.raw.class.getField(fieldName);
				int resourceId = field.getInt(R.raw.class);
				addNote(i,resourceId);
			} catch (Exception e) {
				Log.e(TAG,"invalid note",e);
			}
		}
	}
}