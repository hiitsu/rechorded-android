package com.rechorded.activity;

import android.content.Context;
import android.os.AsyncTask;

class MetronomeTask extends AsyncTask<Context,Void,Void> {
	Context ownerContext;
	long lastTick = System.currentTimeMillis();
	long tickCount = 0;
	boolean stop = false;
	synchronized void finish() {
		stop = true;
	}
	@Override
	protected Void doInBackground(Context... params) {
		ownerContext = params[0];
		while(true) {
			long now = System.currentTimeMillis();
			long diff = now - lastTick;
			if( diff >= 500 ) {
				lastTick = now;
				if( tickCount++ % 2 == 0 )
					SoundManager.getInstance(ownerContext).playMetronomeLow();
				else
					SoundManager.getInstance(ownerContext).playMetronomeHigh();
			}
			if( stop )
				break;
		}
		return null;
	}

}	