package com.rechorded.model;

public class GuitarChord extends Chord {

	private static final long serialVersionUID = 1L;
	String fingers,tuning;
	int barre;
	public GuitarChord(int root, String type, int barre, String fingers,String tuning) {
		super(root, type);
		this.fingers = fingers;
		this.barre = barre;
		this.tuning = tuning;
	}
	private GuitarChord() {}
	public String getFingers() {
		return fingers;
	}
	public void setFingers(String fingers) {
		this.fingers = fingers;
	}
	public int getBarre() {
		return barre;
	}
	public void setBarre(int barre) {
		this.barre = barre;
	}
	public String getTuning() {
		return tuning;
	}
	public void setTuning(String tuning) {
		this.tuning = tuning;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + barre;
		result = prime * result + ((fingers == null) ? 0 : fingers.hashCode());
		result = prime * result + ((tuning == null) ? 0 : tuning.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		GuitarChord other = (GuitarChord) obj;
		if (barre != other.barre)
			return false;
		if (fingers == null) {
			if (other.fingers != null)
				return false;
		} else if (!fingers.equals(other.fingers))
			return false;
		if (tuning == null) {
			if (other.tuning != null)
				return false;
		} else if (!tuning.equals(other.tuning))
			return false;
		return true;
	}



}
