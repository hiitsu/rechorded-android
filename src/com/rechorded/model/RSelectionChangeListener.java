package com.rechorded.model;

public interface RSelectionChangeListener {
	void selectionChanged(Chord chord);
	void changeChordRoot(int root);
	void changeChordType(String type);
}
