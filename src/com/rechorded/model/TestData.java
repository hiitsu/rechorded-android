package com.rechorded.model;

import java.util.ArrayList;

import com.rechorded.filter.RChordRootFilter;
import com.rechorded.filter.RFilter;

import android.database.Cursor;
import android.test.AndroidTestCase;

public class TestData extends AndroidTestCase {
	public void testQueries() {
		Data data = Data.getInstance(getContext());
		//assertTrue(data.getGuitarChordCount() > 0);
		assertTrue(data.getChordTypes().indexOf("maj") != -1);
		assertTrue(data.getScaleTypes().indexOf("dorian") != -1);
		assertTrue(data.getGenres().indexOf("jazz") != -1);
		//assertTrue(data.getGuitarChords(new RFilter).get("Cmaj") != null);
		//assertTrue(data.getGuitarChordCount("root","0") > 0 );
		//assertTrue(data.getGuitarChordCount("type","maj") > 0 );
	}
	public void testNth(){
		Data data = Data.getInstance(getContext());
		ArrayList<RFilter> filters = new ArrayList<RFilter>();
		filters.add(new RChordRootFilter());
		Cursor a = data.nth(filters,1);
		Cursor b = data.nth(filters,2);
		assertNotNull(a);
		assertNotNull(b);
	}
	public void testDefaults() {
//		String q = m.getQuery();
//		assertEquals("SELECT * FROM guitarchord WHERE root=G INTERSECT SELECT * FROM guitarchord WHERE type='maj';",q);
	}
	@Override
	protected void setUp() throws Exception {
		super.setUp();
	}

	@Override
	protected void tearDown() throws Exception {
		super.tearDown();
	}

}
