package com.rechorded.model;

import java.io.Serializable;

public class Chord implements Serializable{
	public final String DEFAULT_TYPE = "maj";
	public final int DEFAULT_ROOT = 0;
	private static final long serialVersionUID = 1L;
	int root;
	String type;
	public Chord(int root, String type) {
		super();
		this.root = root;
		this.type = type;
	}
	public Chord() {
		super();
		this.root = DEFAULT_ROOT;
		this.type = DEFAULT_TYPE;
	}
	public int getRoot() {
		return root;
	}
	public void setRoot(int root) {
		this.root = root;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + root;
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Chord other = (Chord) obj;
		if (root != other.root)
			return false;
		if (type == null) {
			if (other.type != null)
				return false;
		} else if (!type.equals(other.type))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return Data.NOTES[root] + type;
	}
	
}
