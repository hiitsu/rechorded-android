package com.rechorded.model;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class RSQLiteOpenHelper extends SQLiteOpenHelper {
	private static String TAG = "RSQLiteOpenHelper";
	public final static String DATABASE_NAME = "rechorded.sqlite3";
	public final static String DATABASE_PATH = "/data/data/com.rechorded/databases/";
	public final static String DATABASE_FILE = DATABASE_PATH + DATABASE_NAME;
	static final int DATABASE_VERSION = 1;
	SQLiteDatabase dataBase;
	final Context context;
	
	public RSQLiteOpenHelper(Context context) {
	    super(context,DATABASE_NAME,null, DATABASE_VERSION);
	    this.context = context;
        try {
        	getReadableDatabase();
			File file = new File(DATABASE_FILE);
			Log.w(TAG,"Database file "+DATABASE_FILE+" exists flag:"+file.exists());
			boolean flag = file.delete();
			Log.w(TAG,"Database file "+DATABASE_FILE+" delete flag:"+flag);
           	copyDataBase();
            openDataBase();
        } catch (Exception e) {
            throw new Error("Error copying database");
        }
	}

	private void copyDataBase() throws IOException {
		InputStream inputStream = context.getAssets().open(DATABASE_NAME);
		OutputStream outputStream = new FileOutputStream(DATABASE_FILE);
		byte[] buffer = new byte[1024];
		int length;
		while ((length = inputStream.read(buffer))>0){
			outputStream.write(buffer, 0, length);
		}
		outputStream.flush();
		outputStream.close();
		inputStream.close();
	}

	public void openDataBase() throws SQLException {
		dataBase = SQLiteDatabase.openDatabase(DATABASE_FILE, null, SQLiteDatabase.OPEN_READONLY|SQLiteDatabase.NO_LOCALIZED_COLLATORS);
	}
/*
	private boolean dataBaseExists() {
		SQLiteDatabase databaseHandle = null;
		boolean exist = false;
		try {
			databaseHandle = SQLiteDatabase.openDatabase(DATABASE_FILE, null,SQLiteDatabase.OPEN_READONLY|SQLiteDatabase.NO_LOCALIZED_COLLATORS);
		} catch (SQLiteException e) {
		    Log.w(TAG, "database does't exist");
		}
		
		if (databaseHandle != null) {
		    exist = true;
		    databaseHandle.close();
		    databaseHandle = null;
		}
		return exist;
	}*/
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.i(TAG,"onCreate");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.i(TAG,"onUpgrade");
	}

}
