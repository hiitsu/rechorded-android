package com.rechorded.model;

import java.util.ArrayList;
import java.util.Collection;

import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.util.Log;

import com.rechorded.activity.StaticPreferences;
import com.rechorded.filter.RFilter;
import com.rechorded.filter.RTuningFilter;

public class Data extends RSQLiteOpenHelper {

	public static String VALUE_SEP = ":";
	private static String TAG = "Data";
	public static final String[] NOTES = { "C","C#","D","D#","E","F","F#","G","G#","A","A#","B"};
	public static final String[] NOTENUMS = { "0","1","2","3","4","5","6","7","8","9","10","11"};
	public static final String 
			DEFAULT_TUNING = "standard",
			TABLE_GUITARCHORD = "guitarchord",
			TABLE_PIANOCHORD = "chord";

	static final String 
			Q_CHORD = "SELECT root,type,rowid as _id FROM chord :whereClause",
			Q_GROUPED = "SELECT root,type,rowid as _id,COUNT(*) FROM guitarchord :whereClause GROUP BY root, type",
			Q_NTH = "SELECT root,type,barre,fingers,tuning FROM guitarchord :whereClause ORDER BY difficulty ASC LIMIT :nth,1",
			Q_DISTINCT_PIANO_FINGER_USAGE = "SELECT DISTINCT count(num) as n FROM chordstep GROUP BY type ORDER BY n",
			Q_DISTINCT = "SELECT DISTINCT :column FROM :table ORDER BY :column ASC",
			Q_COUNT = "SELECT count(*) FROM :table WHERE :column=:value",
			Q_CHORD2NOTES = "SELECT num FROM chordstep WHERE type=':type'",
			Q_MAX = "SELECT MAX(:column) FROM :table";
	
	static Data instance;

	private Data(Context c) {
		super(c);
	}
	public static String toRoot(String index){
		int i = -1;
		try{
			i = Integer.parseInt(index);
		} catch(NumberFormatException e) {
			return index;
		}
		return i == -1 ? null : NOTES[i];
	}
	static int creationCounter = 0;
	public static synchronized Data getInstance(Context c){
		if( instance == null ) {
			instance = new Data(c);
			creationCounter++;
			if( creationCounter > 1 ){
				throw new Error("Data instance created for the second time");
			}
		}
		return instance;
	}
	public List<String> getPianoChordFingerUsages() {
		Cursor cursor = timedCursor(Q_DISTINCT_PIANO_FINGER_USAGE);
		return cursorToList(cursor,new int[]{0});
	}
	public List<String> getDistinct(String table,String column) {
		String q = Q_DISTINCT.replace(":table",table).replace(":column",column);
		Cursor cursor = timedCursor(q);
		return cursorToList(cursor,new int[]{0});
	}
	public long getCount(String table,String column,String value,boolean stringFlag) {
		String q = Q_COUNT.replace(":table",table).replace(":column",column).replace(":value",stringFlag ? "'"+value+"'" : value);
		Cursor cursor = timedCursor(q);
		long num = cursor.getLong(0);
		cursor.close();
		return num;
	}
	public long getMax(String table,String column) {
		String q = Q_MAX.replace(":table",table).replace(":column",column);
		Cursor cursor = timedCursor(q);
		long num = cursor.getLong(0);
		cursor.close();
		return num;
	}
	public List<String> getChordTypes() {
		return getDistinct("chordtype","name");
	}
	public List<String> getScaleTypes() {
		return getDistinct("scaletype","name");
	}
	public List<String> getGenres() {
		return getDistinct("genre","name");
	}
	public GuitarChord nthGuitarChord(Collection<RFilter> filters,int n){
		Cursor cursor = nth(filters,n);
		if( cursor == null || cursor.isClosed() || cursor.getCount() <= 0 )
			return null;
		return new GuitarChord(cursor.getInt(0),cursor.getString(1),cursor.getInt(2),cursor.getString(3),cursor.getString(4));
	}
	public Cursor nth(Collection<RFilter> filters,int n){
		List<RFilter> list = new ArrayList<RFilter>(filters);
		list.add(new RTuningFilter(StaticPreferences.tuning));
		String whereClause = whereClause(list);
		String q = Q_NTH
				.replace(":whereClause",whereClause)
				.replace(":nth",String.valueOf(n));
		Cursor cursor = timedCursor(q);
		return cursor;
	}

	public static String cursorToString(Cursor cursor,int[] columns){
		if( cursor.isClosed() ) {
			Log.e(TAG,"cursorToString cursor is closed");
			return null;
		}
		if( cursor.isBeforeFirst() || cursor.isAfterLast() ) {
			Log.e(TAG,"cursorToString invalid cursor position");
			return null;
		}
		int c = cursor.getColumnCount();
		String s = "";
		for(int j=0;j < columns.length; j++)
			for(int i=0; i < c; i++)
				if( i == columns[j] )
						s += cursor.getString(i) + VALUE_SEP;
		return s.substring(0,s.length()-VALUE_SEP.length());
	}

	public Cursor getGroupedGuitarChordCursor(Collection<RFilter> filters){
		List<RFilter> list = new ArrayList<RFilter>(filters);
		list.add(new RTuningFilter(StaticPreferences.tuning));
		String whereClause = whereClause(list);
		String q = Q_GROUPED.replace(":whereClause",whereClause);
		Cursor cursor = timedCursor(q);
		return cursor;
	}
	public Cursor getPianoChords(Collection<RFilter> filters) {
		String whereClause = whereClause(filters);
		String q = Q_CHORD.replace(":whereClause",whereClause);
		Cursor cursor = timedCursor(q);
		return cursor;
	}
	public Cursor timedCursor(String q) {
		long started,stopped;
		started = System.currentTimeMillis();
		Cursor cursor = dataBase.rawQuery(q,null);
		stopped = System.currentTimeMillis();
		Log.w(TAG,q+ " took "+((stopped-started))+ " milliseconds and has "+cursor.getCount() +" rows");
		if( !cursor.moveToFirst() ) {
			cursor.close();
			return null;
		}
		return cursor;
	}
	public String whereClause(Collection<RFilter> filters) {
		String whereClause = "WHERE ", AND = " AND ";
		for(RFilter filter : filters) {
			if( !filter.isEnabled() )
				continue;
			whereClause += filter.whereClause();
			whereClause += AND;
		}
		whereClause = whereClause.substring(0,whereClause.length()-AND.length());
		return whereClause;
	}
	
	private List<String> cursorToList(Cursor cursor,int[] columns){
		List<String> list = new ArrayList<String>();
		for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			list.add(cursorToString(cursor,columns));
		}
		cursor.close();
		return list;
	}
	public List<String> getChordTypeSteps(String type) {
		String q = Q_CHORD2NOTES.replace(":type",type);
		Cursor cursor = timedCursor(q);
		List<String> list = cursorToList(cursor,new int[]{0});
		return list;
	}

}

/*
 * 	private void cursorToNothing(Cursor cursor) {
		for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			
		}
	}
 	private ChordResultMap cursorToMap(Cursor cursor,int textCols[],int valueCols[]){
		ChordResultMap map = new ChordResultMap();
		for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
			String text = "";
			for( int textCol : textCols ) {
				String columnName = cursor.getColumnName(textCol);
				if( columnName.equalsIgnoreCase("root") )
					text += NOTES[Integer.parseInt(cursor.getString(textCol))];
				else
					text += cursor.getString(textCol);
			}
			String value = "";
			for( int valueCol : valueCols ) {
				value += cursor.getString(valueCol) + VALUE_SEP;
			}
			if( !map.containsKey(text) )
				map.put(text,new ArrayList<String>());
			map.get(text).add(value);
		}
		cursor.close();
		return map;
	}
 * public Cursor getGuitarChordCursor(Collection<RFilter> filters){
if( filters.size() == 0 ) {
	Log.w(TAG,"filters.size() == 0");
	return null;
}
String q = "";
for(RFilter filter : filters) {
	q += filter.sql();
	q += INTERSECT;
}
q = q.substring(0,q.length()-INTERSECT.length()) +";";
Log.d(TAG,q);
long started,stopped;
started = System.currentTimeMillis();
Cursor cursor = dataBase.rawQuery(q,null);
stopped = System.currentTimeMillis();
Log.w(TAG,q+ " took "+((stopped-started)));
return cursor;
}
public ChordResultMap getGuitarChords() {
long started,stopped;

Cursor cursor = getGroupedGuitarChordCursor();
if( cursor == null )
	return new ChordResultMap();
started = System.currentTimeMillis();
cursorToNothing(cursor);
stopped = System.currentTimeMillis();
Log.w(TAG,"cursorToNothing took "+(stopped-started));

started = System.currentTimeMillis();
ChordResultMap map = cursorToMap(cursor,new int[]{0,1},new int[]{0,1,2,4});
stopped = System.currentTimeMillis();
Log.w(TAG,"cursorToMap took "+(stopped-started));
return map;
}
*/

/*
 * 	public String generateWhereClause(Collection<String> fieldNames) {
		StringBuffer buffer = new StringBuffer("");
		for( String s : fieldNames ) {
			buffer.append(s);
			buffer.append("=? AND ");
		}
		return buffer.substring(0,buffer.length()-5);
	}
 	final String FILTER_BY_SCALE = "SELECT root,type,barre,fingers FROM guitarchord " +
			"WHERE root IN (SELECT chordroot FROM scale2chord WHERE scaleroot=:scaleroot AND scaletype=':scaletype') " +
			"AND type IN (SELECT chordtype FROM scale2chord WHERE scaleroot=:scaleroot AND scaletype=':scaletype');";
	final String FILTER_BY_SCALE_AND_CHORDTYPE = 
			"SELECT DISTINCT root,type,barre,fingers FROM guitarchord " +
			"WHERE root IN (SELECT chordroot FROM scale2chord WHERE scaleroot=:scaleroot AND scaletype=':scaletype') " +
			"AND type IN (SELECT chordtype FROM scale2chord WHERE scaleroot=:scaleroot AND scaletype=':scaletype') " +
			"INTERSECT SELECT root,type,barre,fingers FROM guitarchord WHERE type=':chordtype'";
	final String FILTER_BY_SCALE_AND_CHORDROOT = 
			"SELECT DISTINCT root,type,barre,fingers FROM guitarchord " +
			"WHERE root IN (SELECT chordroot FROM scale2chord WHERE scaleroot=:scaleroot AND scaletype=':scaletype') " +
			"AND type IN (SELECT chordtype FROM scale2chord WHERE scaleroot=:scaleroot AND scaletype=':scaletype') " +
			"INTERSECT SELECT root,type,barre,fingers FROM guitarchord WHERE root=:chordroot";
	final String FILTER_BY_CHORDROOT = 
			"SELECT DISTINCT root,type,barre,fingers FROM guitarchord WHERE root=:chordroot";
	final String FILTER_BY_CHORDTYPE = 
			"SELECT DISTINCT root,type,barre,fingers FROM guitarchord WHERE type=:chordtype";
*/