package com.rechorded.model;

import android.database.Cursor;

public interface RFilterChangeListener {
	void filterChanged(int filterId,Object value,Cursor cursor);
}
