package com.takakontti;

import android.content.Context;
import android.graphics.Color;
import android.view.View;

public class Separator extends View {

	public Separator(Context context) {
		super(context);
		setBackgroundColor(Color.RED);
	}

}
