package com.takakontti;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class HorizontalSeparator extends Separator {

	public HorizontalSeparator(Context context) {
		super(context);
		setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT,5));
	}

}
