package com.takakontti;

import android.content.Context;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class VerticalSeparator extends Separator {

	public VerticalSeparator(Context context) {
		super(context);
		setLayoutParams(new LinearLayout.LayoutParams(5,LayoutParams.MATCH_PARENT));
	}

}
