package com.takakontti;


import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

public class AutoFitArrayAdapter extends ArrayAdapter<String>{
    Context context;
    String[] values;

    public AutoFitArrayAdapter(Context context, int textViewResourceId,
            String[] values) {
        super(context, textViewResourceId, values);
        this.context = context;
        this.values = values;
    }

    public int getCount(){
       return values.length;
    }

    public String getItem(int position){
       return values[position];
    }

    public long getItemId(int position){
       return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AutoFitTextView label = new AutoFitTextView(context);
        label.setTextColor(Color.BLACK);
        label.setText(values[position]);
        label.setPadding(0, 0, 0, 0);
        return label;
    }


    @Override
    public View getDropDownView(int position, View convertView,ViewGroup parent) {
        return super.getDropDownView(position, convertView, parent);
    }
}